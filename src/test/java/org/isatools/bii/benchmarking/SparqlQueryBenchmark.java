package org.isatools.bii.benchmarking;

import static org.junit.Assert.assertTrue;
import info.aduna.iteration.CloseableIteration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptException;

import org.apache.commons.io.FileDeleteStrategy;
import org.isatools.database.SparqlQuery;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

/**
 * Created by the ISATeam. User: agbeltran Date: 27/06/2013 Time: 16:48
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
@RunWith(value = Parameterized.class)
public class SparqlQueryBenchmark {

	private static final int QUERY_EVAL_RUN_TIMES = 10;
	private boolean PRINT_RESULTS = false;
	private static SailConnection neo4jSailConnection;
	private static SailConnection tinkerSailConnection;
	private static SailConnection orientSailConnection;
	private static String baseURI;
	private static String[] sparqlQueries;
	private static Map<String, String> variablesValues = new HashMap<String, String>();
	private static SparqlQuery sparqlQueryObj;

	@BeforeClass
	public static void testLoading() throws SailException, IOException {
		File benchmark = new File(SparqlQueryBenchmark.class.getClass()
				.getResource("/benchmark").getFile());
		for (File f : benchmark.listFiles()) {
			if (!f.getName().equals("create.txt"))
				FileDeleteStrategy.FORCE.delete(f);
		}

		Neo4jGraph neo4jGraph = new Neo4jGraph(benchmark.getPath() + "/neo4j");
		OrientGraph orientGraph = new OrientGraph("local:"
				+ benchmark.getPath() + "/orientGraph");
		TinkerGraph tinkerGraph = new TinkerGraph(benchmark.getPath()
				+ "/tinkergraph");

		GraphSail<Neo4jGraph> neo4jSail = new GraphSail<Neo4jGraph>(neo4jGraph);
		GraphSail<TinkerGraph> tinkerSail = new GraphSail<TinkerGraph>(
				tinkerGraph);
		GraphSail<OrientGraph> orientSail = new GraphSail<OrientGraph>(
				orientGraph);

		Sail neo4jReasoner = new ForwardChainingRDFSInferencer(neo4jSail);
		Sail tinkerReasoner = new ForwardChainingRDFSInferencer(tinkerSail);
		Sail orientReasoner = new ForwardChainingRDFSInferencer(orientSail);

		SailGraph neo4jSailGraph = new SailGraph(neo4jSail);
		SailGraph tinkerSailGraph = new SailGraph(tinkerSail);
		SailGraph orientSailGraph = new SailGraph(orientSail);

		neo4jSailConnection = neo4jReasoner.getConnection();
		tinkerSailConnection = tinkerReasoner.getConnection();
		orientSailConnection = orientReasoner.getConnection();

		baseURI = "http://isa-tools.org/isa/";
		neo4jSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		neo4jSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		tinkerSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		orientSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		neo4jSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(SparqlQueryBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);

		neo4jGraph.getVertices();
		neo4jGraph.getEdges();
		orientGraph.getVertices();
		orientGraph.getEdges();
		tinkerGraph.getVertices();
		tinkerGraph.getEdges();
		variablesValues.put("$i_id", "BII-I-1");
		variablesValues.put("$s_id", "BII-S-1");
	}

	public SparqlQueryBenchmark(String[] sparqlQueries) {
		this.sparqlQueries = sparqlQueries;
	}

	@Parameters
	public static Collection<Object[]> data() {
		sparqlQueryObj = new SparqlQuery();
		Object[][] data = new Object[][] {
				{ new String[] { sparqlQueryObj.ASSAY_QUERY,
						sparqlQueryObj.STUDY_ASSAY_QUERY } },
				{ new String[] { sparqlQueryObj.BROWSE_QUERY,
						sparqlQueryObj.STUDY_ASSAY_QUERY } },
				{ new String[] { sparqlQueryObj.INVESTIGATION_QUERY,
						sparqlQueryObj.INVESTIGATION_CONTACTS_QUERY } },
				{ new String[] { sparqlQueryObj.STUDY_QUERY,
						sparqlQueryObj.STUDY_CONTACTS_QUERY,
						sparqlQueryObj.STUDY_GROUPS_COUNT } },
				{ new String[] { sparqlQueryObj.STUDY_BROWSE_VIEW,
						sparqlQueryObj.STUDY_ASSAYS } },
				{ new String[] { sparqlQueryObj.STUDY_BROWSE_VIEW,
						sparqlQueryObj.STUDY_ASSAYS } } };
		return Arrays.asList(data);
	}

	@Test
	public void testQuery() throws ScriptException, MalformedQueryException,
			SailException, QueryEvaluationException {

		HashMap<String, Double> durations = new HashMap<String, Double>();
		String queryNames = "";
		SPARQLParser parser = new SPARQLParser();
		List<ParsedQuery> parsedQueries = new ArrayList<ParsedQuery>();

		for (String query : sparqlQueries) {
			String stringQuery = sparqlQueryObj.parseFilesToString(
					sparqlQueryObj.PREFIX, query);
			for (String variable : variablesValues.keySet()) {
				stringQuery = stringQuery.replace(variable,
						variablesValues.get(variable));
			}
			queryNames += query.substring(query.lastIndexOf("/") + 1);
			ParsedQuery parsedQuery = parser.parseQuery(stringQuery, baseURI);
			parsedQueries.add(parsedQuery);
		}

		long startTime = 0, total = 0;

		/**
		 * Neo4J Performance
		 */
		total = 0;
		List<CloseableIteration<? extends BindingSet, QueryEvaluationException>> neo4jResults = new ArrayList<CloseableIteration<? extends BindingSet, QueryEvaluationException>>();

		int tinkerR = 0, orientR = 0, neoR = 0;

		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.currentTimeMillis();
			for (ParsedQuery parsedQuery : parsedQueries) {
				neo4jResults.add(neo4jSailConnection.evaluate(
						parsedQuery.getTupleExpr(), parsedQuery.getDataset(),
						new EmptyBindingSet(), false));
				for (CloseableIteration<? extends BindingSet, QueryEvaluationException> neo4jResult : neo4jResults) {
					while (neo4jResult.hasNext()) {
						BindingSet neoRes = neo4jResult.next();
						for (String name : neoRes.getBindingNames()) {
							neoR += neoRes.getBinding(name).getValue()
									.stringValue().length();
						}
					}
				}
			}
			total += System.currentTimeMillis() - startTime;
		}

		System.out.println("--------------SPARQL RESULTS-----------------");

		System.out.println("Neo4J " + queryNames + " Duration "
				+ (double) (total / QUERY_EVAL_RUN_TIMES) + "ms");
		System.out.println("---------------------------------------------");

		/**
		 * Tinker Graph evalution
		 */
		startTime = 0;
		total = 0;
		List<CloseableIteration<? extends BindingSet, QueryEvaluationException>> tinkerResults = new ArrayList<CloseableIteration<? extends BindingSet, QueryEvaluationException>>();
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.currentTimeMillis();
			for (ParsedQuery parsedQuery : parsedQueries) {
				tinkerResults.add(tinkerSailConnection.evaluate(
						parsedQuery.getTupleExpr(), parsedQuery.getDataset(),
						new EmptyBindingSet(), false));
				for (CloseableIteration<? extends BindingSet, QueryEvaluationException> tinkerResult : tinkerResults) {
					while (tinkerResult.hasNext()) {
						BindingSet tinkerRes = tinkerResult.next();
						for (String name : tinkerRes.getBindingNames()) {
							tinkerR += tinkerRes.getBinding(name).getValue()
									.stringValue().length();
						}
					}
				}
			}
			total += System.currentTimeMillis() - startTime;
		}
		System.out.println("--------------SPARQL RESULTS-----------------");

		System.out.println("TinkerGraph " + queryNames + " Duration "
				+ (double) (total / QUERY_EVAL_RUN_TIMES) + "ms");
		System.out.println("---------------------------------------------");

		/**
		 * Orient Graph evalution
		 */
		startTime = 0;
		total = 0;
		List<CloseableIteration<? extends BindingSet, QueryEvaluationException>> orientResults = new ArrayList<CloseableIteration<? extends BindingSet, QueryEvaluationException>>();
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.currentTimeMillis();
			for (ParsedQuery parsedQuery : parsedQueries) {
				orientResults.add(orientSailConnection.evaluate(
						parsedQuery.getTupleExpr(), parsedQuery.getDataset(),
						new EmptyBindingSet(), false));
				for (CloseableIteration<? extends BindingSet, QueryEvaluationException> orientResult : orientResults) {
					while (orientResult.hasNext()) {
						BindingSet orientRes = orientResult.next();
						for (String name : orientRes.getBindingNames()) {
							orientR += orientRes.getBinding(name).getValue()
									.stringValue().length();
						}
					}
				}
			}
			total += System.currentTimeMillis() - startTime;
		}
		System.out.println("--------------SPARQL RESULTS-----------------");

		System.out.println("OrientGraph " + queryNames + " Duration "
				+ (double) (total / QUERY_EVAL_RUN_TIMES) + "ms");
		System.out.println("---------------------------------------------");

		assertTrue((tinkerR == orientR) && tinkerR == neoR);
	}

}