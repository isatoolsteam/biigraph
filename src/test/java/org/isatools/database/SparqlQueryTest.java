package org.isatools.database;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.isatools.database.structures.QueryResultsIterator;
import org.isatools.parser.QuerytoJSON;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openrdf.query.QueryEvaluationException;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/**
 * Created by the ISATeam. User: agbeltran Date: 16/07/2013 Time: 10:55
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
public class SparqlQueryTest {

	private static SparqlQuery query;

	@BeforeClass
	public static void init() {
		query = new SparqlQuery();
	}

	@Test
	public void existsIDTest() throws QueryEvaluationException {
		int x = query.existsID("BII-I-1");
		assertEquals(x, 1);
		x = query.existsID("BII-S-1");
		assertEquals(x, 2);
		x = query.existsID("BII-I-3");
		assertEquals(x, -1);
		x = query.existsID("BII-S-3");
		assertEquals(x, -1);

	}

	// BROWSE PAGE
	@Test
	public void browseTest() {
		query.printQueryResults(query
				.runQuery(query.PREFIX, query.BROWSE_QUERY));
		JsonObject json = query.getBrowseInfo();
		System.out.println("JSON = " + json);
	}

    @Test
    public void getBrowse2Test() {
        Map<String, String> variablesValues = new HashMap<String, String>();
        variablesValues.put("$s_id", "BII-S-1");
        System.out.println("query.PREFIX="+query);
        query.printQueryResults(query.runQuery(query.PREFIX,
                query.BROWSE_QUERY_PATH + "browse.sparql", variablesValues));
    }

    @Test
    public void getBrowseWellDesignedTest() {
        Map<String, String> variablesValues = new HashMap<String, String>();
        variablesValues.put("$s_id", "BII-S-1");
        System.out.println("query.PREFIX="+query);
        query.printQueryResults(query.runQuery(query.PREFIX,
                query.BROWSE_QUERY_PATH + "browse_well_designed.sparql", variablesValues));
    }

	@Test
	public void getStudyBrowseViewTest() {
		JsonObject json = query.getStudyBrowseView("BII-S-1");
		System.out.println("JSON = " + json);
	}

	@Test
	public void assayTypesCountTest() {
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.BROWSE_QUERY_PATH + "assay_types_count_per_study.sparql"));
	}

	// INVESTIGATION PAGE
	@Test
	public void getInvestigationTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", "BII-I-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.INVESTIGATION_QUERY, variablesValues));

		JsonObject json = query.getInvestigationInfo("BII-I-1");
		System.out.println("JSON = " + json);
	}

	@Test
	public void getInvestigationIDfromStudyIDTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.INVESTIGATION_ID_FROM_STUDY_ID, variablesValues));
	}

	@Test
	public void getContactInvestigationsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", "BII-I-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.INVESTIGATION_CONTACTS_QUERY, variablesValues));
	}

	@Test
	public void getInvestigationStudiesTest() {
		JsonObject json = query.getInvestigationStudiesBrowseView("BII-I-1");
		System.out.println("JSON = " + json);
	}

	// UPDATE
	// @Test
	public void updateInvestigationTest() {
		query.updateInvestigation("BII-I-1", "i_title", "aaa");
		JsonObject s = query.getInvestigationInfo("BII-I-1");
		assertEquals("aaa", s.get("i_title").getAsString());
		query.updateInvestigation("BII-I-1", "i_description", "asaa");
		s = query.getInvestigationInfo("BII-I-1");
		assertEquals("asaa", s.get("i_description").getAsString());
	}

	// DELETE
	@Test
	public void deleteInvestigationTest() {
		query.deleteInvestigation("BII-I-1");
		JsonObject s = query.getInvestigationInfo("BII-I-1");
		assertEquals(null, s);
		s = query.getStudyInfo("BII-S-1");
		assertEquals(null, s);
		System.out.println(query.getBrowseInfo());
	}

	@Test
	public void deleteInvestigationContactsTest() {
		String queryString = query.parseFilesToString(query.PREFIX,
				query.DELETE_INVESTIGATION_CONTACTS_QUERY);
		queryString = queryString.replace("$i_id", "BII-I-1");
		query.runUpdateQuery(queryString);
		JsonObject s = query.getInvestigationInfo("BII-I-1");
		JsonArray contacts = s.get("i_contacts").getAsJsonArray();
		assertEquals(0, contacts.size());
	}

	// STUDY PAGE
	@Test
	public void getStudyTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		long start = System.currentTimeMillis();
		QueryResultsIterator results = query.runQuery(query.PREFIX,
				query.STUDY_QUERY_PATH + "study.sparql", variablesValues);

		System.out.println(System.currentTimeMillis() - start);

		JsonObject json = query.getStudyInfo("BII-S-1");
		System.out.println("JSON = " + json);
	}

	@Test
	public void getStudy2Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-2");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study.sparql", variablesValues));
	}

	@Test
	public void getStudyContactsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_QUERY_PATH + "study_contacts.sparql",
				variablesValues));
	}

	@Test
	public void studyGroupsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_GROUPS_COUNT, variablesValues));
	}

	@Test
	public void studyGroups2Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-2");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study/study_groups_count.sparql", variablesValues));
	}

	@Test
	public void studyGroups3Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "MTBLS2");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_GROUPS_COUNT, variablesValues));
		// query.QUERY_PATH + "study/study_groups.sparql", variablesValues));
	}

	@Test
	public void studyGroupMembersTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues
				.put("$study_group",
						"Factor Value[limiting nutrient] RID:nitrogen Factor Value[rate] 0.2 Unit l/hour");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study/study_group_members.sparql", variablesValues));
	}

	@Test
	public void studyGroupMembers2Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues
				.put("$study_group",
						"Factor Value[limiting nutrient] RID:sulfur Factor Value[rate] 0.1 Unit l/hour");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study/study_group_members.sparql", variablesValues));
	}

	@Test
	public void studyGroupsAndTheirCountTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study/study_groups_count.sparql", variablesValues));
	}

	// UPDATE
	// @Test
	public void updateStudyTest() {
		query.updateStudy("BII-S-1", "s_title", "aaa");
		JsonObject s = query.getStudyInfo("BII-S-1");
		assertEquals("aaa", s.get("s_title").getAsString());
		query.updateStudy("BII-S-1", "s_description", "aasa");
		s = query.getStudyInfo("BII-S-1");
		assertEquals("aasa", s.get("s_description").getAsString());
	}

	// DELETE
	@Test
	public void deleteStudyTest() {
		query.deleteStudy("BII-S-1", false);
		query.deleteStudy("BII-S-2", false);
		JsonObject s = query.getStudyInfo("BII-S-1");
		assertEquals(null, s);
		System.out.println(query.getBrowseInfo());
	}

	@Test
	public void deleteStudiesContactsTest() {
		String queryString = query.parseFilesToString(query.PREFIX,
				query.DELETE_STUDY_CONTACTS_QUERY);
		queryString = queryString.replace("$s_id", "BII-S-1");
		query.runUpdateQuery(queryString);
		JsonObject s = query.getStudyInfo("BII-S-1");
		JsonArray contacts = s.get("s_contacts").getAsJsonArray();
		assertEquals(0, contacts.size());
	}

	// ASSAY PAGE
	@Test
	public void assayDetailsTest() {
		long start=System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		variablesValues.put("$measurement", "OBI:metabolite profiling");
		variablesValues.put("$technology", "OBI:mass spectrometry");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assay/assay_type_details.sparql", variablesValues));
		System.out.println(System.currentTimeMillis()-start);
		
	}
	
	@Test
	public void assayDetailsInfoTest() {
		JsonObject json = new SparqlQuery().getAssayTypeDetails( "E-GEOD-25835-MP", "OBI:transcription profiling", "OBI:DNA microarray");
		System.out.println(json);
		
	}

	@Test
	public void assayDetails2Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		variablesValues.put("$measurement", "OBI:metabolite profiling");
		variablesValues.put("$technology", "OBI:mass spectrometry");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assay/assay_type_details.sparql", variablesValues));
	}

	@Test
	public void assayDetails3Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-2");
		variablesValues.put("$measurement", "OBI:transcription profiling");
		variablesValues.put("$technology", "OBI:DNA microarray");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assay/assay_type_details.sparql", variablesValues));
	}

	@Test
	public void assayDetails4Test() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "E-GEOD-25835-MP");
		variablesValues.put("$measurement", "OBI:transcription profiling");
		variablesValues.put("$technology", "OBI:DNA microarray");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assay/assay_type_details.sparql", variablesValues));
	}

	@Test
	public void assaySamplesTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "MTBLS2");
		variablesValues.put("$measurement", "OBI:metabolite profiling");
		variablesValues.put("$technology", "OBI:mass spectrometry");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assay/assay_type_samples.sparql", variablesValues));
	}

    @Test
    public void assayDetails5Test() {
        Map<String, String> variablesValues = new HashMap<String, String>();
        variablesValues.put("$s_id", "BII-S-3");
        variablesValues.put("$measurement", "OBI:transcription profiling");
        variablesValues.put("$technology", "OBI:nucleotide sequencing");
        query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
                + "assay/assay_type_details.sparql", variablesValues));
    }


	@Test
	public void assayDetailsParseTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		variablesValues.put("$measurement", "OBI:transcription profiling");
		variablesValues.put("$technology", "OBI:DNA microarray");
		QueryResultsIterator results = query.runQuery(query.PREFIX,
				query.QUERY_PATH + "assay/assay_type_details.sparql",
				variablesValues);
		JsonObject json = QuerytoJSON.parseAssayDetails(results);
		System.out.println(json);
	}

	@Test
	public void sampleDetailsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$sample", "Ex1-Col0-48h-Ag-1 Sample");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "sample/sample_details.sparql", variablesValues));
	}

	@Test
	public void derivesTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "derived_from_source.sparql", variablesValues));
	}

	@Test
	public void derivesMaterialTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "material_derived_from_source.sparql", variablesValues));
	}

    //SAMPLE
    @Test
    public void getSampleTest() {
        Map<String, String> variablesValues = new HashMap<String, String>();
        variablesValues.put("$sample", "N-0.1-aliquot7 Sample");
        System.out.println("query.PREFIX="+query.PREFIX);
        query.printQueryResults(query.runQuery(query.PREFIX,
                query.SAMPLE_QUERY_PATH + "sample.sparql", variablesValues));
    }


	@Test
	public void getStudyInfoBenchmarkTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "MTBLS2");
		long start = System.currentTimeMillis();
		QueryResultsIterator studyResults = query.runQuery(query.PREFIX,
				query.STUDY_QUERY, variablesValues);
		System.out.println(query.STUDY_QUERY + " "
				+ (System.currentTimeMillis() - start));
		start = System.currentTimeMillis();
		QueryResultsIterator studyContactsResults = query.runQuery(
				query.PREFIX, query.STUDY_CONTACTS_QUERY, variablesValues);
		System.out.println(query.STUDY_CONTACTS_QUERY + " "
				+ (System.currentTimeMillis() - start));

		start = System.currentTimeMillis();
		QueryResultsIterator studyGroupsResults = query.runQuery(query.PREFIX,
				query.STUDY_GROUPS_COUNT, variablesValues);
		System.out.println(query.STUDY_GROUPS_COUNT + " "
				+ (System.currentTimeMillis() - start));

		start = System.currentTimeMillis();
		QuerytoJSON.parseStudyResults(studyResults, studyContactsResults,
				studyGroupsResults, query);
		System.out.println("SPARQLtoJSON "
				+ (System.currentTimeMillis() - start));
	}

	@Test
	public void getInvestigationInfoBenchmarkTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-I-1");
		long start = System.currentTimeMillis();
		QueryResultsIterator invResults = query.runQuery(query.PREFIX,
				query.INVESTIGATION_QUERY, variablesValues);
		System.out.println(query.STUDY_QUERY + " "
				+ (System.currentTimeMillis() - start));
		start = System.currentTimeMillis();
		QueryResultsIterator invContactsResults = query.runQuery(query.PREFIX,
				query.INVESTIGATION_CONTACTS_QUERY, variablesValues);
		System.out.println(query.STUDY_CONTACTS_QUERY + " "
				+ (System.currentTimeMillis() - start));

		start = System.currentTimeMillis();
		QuerytoJSON.parseInvestigationResults(invResults, invContactsResults);
		System.out.println("SPARQLtoJSON "
				+ (System.currentTimeMillis() - start));
	}

	@Test
	public void getStudyAssaysInfoTest() {
		JsonObject json = query.getStudyAssayInfo("BII-S-1");
		System.out.println("JSON = " + json);
	}

	// @Test
	public void contactsTest() {
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "contacts.sparql"));
	}

	// @Test
	public void sourcesCharacteristicsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "sources_characteristics.sparql", variablesValues));
	}

//	@Test
	public void assaysTest() {
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assays.sparql"));
	}

	// @Test
	public void assaysPerStudyCountTest() {
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "assays_per_study_count.sparql"));
	}

	// @Test
	public void studyAssaysTest() {
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "study_assays.sparql"));
	}

	@Test
	public void subclassTest() {
		query.printQueryResults(query.runQuery(query.PREFIX, query.QUERY_PATH
				+ "subclass.sparql"));
	}

}
