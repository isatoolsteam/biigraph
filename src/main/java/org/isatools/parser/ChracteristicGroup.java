package org.isatools.parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class ChracteristicGroup {
	
	private HashMap<String,String> characteristics;
	private List<StudyGroup> studyGroups;
	private String organism;
	
	public ChracteristicGroup(){
		this.characteristics=new HashMap<String, String>();
		this.studyGroups = new ArrayList<StudyGroup>();
	}


	public String get(String characteristic_type) {
		return characteristics.get(characteristic_type);
	}


	public void put(String characteristic_type, String charvalue) {
		if(characteristic_type.equals("organism"))
			organism=charvalue;
		this.characteristics.put(characteristic_type, charvalue);
	}


	public boolean contains(String characteristic_type) {
		return characteristics.get(characteristic_type)!=null;
	}


	public JsonElement toJsonObject() {
		JsonArray charArray=new JsonArray();
		characteristics.remove("organism");
		for (Entry<String,String> characteristic : characteristics.entrySet()) {
			JsonObject json = new JsonObject();
			json.addProperty("type", characteristic.getKey());
			json.addProperty("value", characteristic.getValue());
			charArray.add(json);
		}
		JsonArray studyGroupsArr=new JsonArray();
		for (StudyGroup studyGroup : studyGroups) {
			studyGroupsArr.add(studyGroup.toJsonObject());
		}
		
		JsonObject obj=new JsonObject();
		obj.add("characteristics", charArray);
		obj.addProperty("organism", organism);
		obj.add("study_groups", studyGroupsArr);
		return obj;
	}


	public void addStudyGroup(StudyGroup study_group) {
		this.studyGroups.add(study_group);
	}
	
	
	
	
	
}
