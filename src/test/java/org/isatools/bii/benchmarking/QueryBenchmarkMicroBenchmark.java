package org.isatools.bii.benchmarking;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

import org.apache.commons.io.FileDeleteStrategy;
import org.isatools.database.GremlinQuery;
import org.isatools.database.SparqlQuery;
import org.isatools.database.structures.QueryResultsIterator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import bb.util.Benchmark;

import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;
import com.tinkerpop.gremlin.groovy.GremlinGroovyPipeline;

/**
 * Created by the ISATeam. User: agbeltran Date: 27/06/2013 Time: 16:48
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
@RunWith(value = Parameterized.class)
public class QueryBenchmarkMicroBenchmark {

	private static final int QUERY_EVAL_RUN_TIMES = 1;
	private boolean PRINT_RESULTS = false;
	private static SailConnection neo4jSailConnection;
	private static SailConnection tinkerSailConnection;
	private static SailConnection orientSailConnection;
	private static String baseURI;
	private static ScriptEngine neo4jEngine;
	private static ScriptEngine tinkerEngine;
	private static ScriptEngine orientEngine;
	private static String[] sparqlQuery;
	private static String[] gremlinScript;
	private static Map<String, String> variablesValues = new HashMap<String, String>();
	private static SparqlQuery sparqlQueryObj;
	private static GremlinQuery gremlinQueryObj;

	@BeforeClass
	public static void testLoading() throws SailException, IOException {
		File benchmark = new File(QueryBenchmarkMicroBenchmark.class.getClass()
				.getResource("/benchmark").getFile());
		for (File f : benchmark.listFiles()) {
			if (!f.getName().equals("create.txt"))
				FileDeleteStrategy.FORCE.delete(f);
		}

		Neo4jGraph neo4jGraph = new Neo4jGraph(benchmark.getPath() + "/neo4j");
		OrientGraph orientGraph = new OrientGraph("local:"
				+ benchmark.getPath() + "/orientGraph");
		TinkerGraph tinkerGraph = new TinkerGraph(benchmark.getPath()
				+ "/tinkergraph");

		GraphSail<Neo4jGraph> neo4jSail = new GraphSail<Neo4jGraph>(neo4jGraph);
		GraphSail<TinkerGraph> tinkerSail = new GraphSail<TinkerGraph>(
				tinkerGraph);
		GraphSail<KeyIndexableGraph> orientSail = new GraphSail<KeyIndexableGraph>(
				orientGraph);

		Sail neo4jReasoner = new ForwardChainingRDFSInferencer(neo4jSail);
		Sail tinkerReasoner = new ForwardChainingRDFSInferencer(tinkerSail);
		Sail orientReasoner = new ForwardChainingRDFSInferencer(orientSail);

		SailGraph neo4jSailGraph = new SailGraph(neo4jSail);
		SailGraph tinkerSailGraph = new SailGraph(tinkerSail);
		SailGraph orientSailGraph = new SailGraph(orientSail);

		neo4jSailConnection = neo4jReasoner.getConnection();
		tinkerSailConnection = tinkerReasoner.getConnection();
		orientSailConnection = orientReasoner.getConnection();

		baseURI = "http://isa-tools.org/isa/";
		neo4jSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		neo4jSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		tinkerSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		orientSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		neo4jSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(QueryBenchmarkMicroBenchmark.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);

		ScriptEngineManager manager = new ScriptEngineManager();
		neo4jEngine = manager.getEngineByName("gremlin-groovy");
		neo4jEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				neo4jSailGraph);

		tinkerEngine = manager.getEngineByName("gremlin-groovy");
		tinkerEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				tinkerSailGraph);

		orientEngine = manager.getEngineByName("gremlin-groovy");
		orientEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				orientSailGraph);

		neo4jGraph.getVertices();
		neo4jGraph.getEdges();
		orientGraph.getVertices();
		orientGraph.getEdges();
		tinkerGraph.getVertices();
		tinkerGraph.getEdges();
		variablesValues.put("$i_id", "BII-I-1");
		variablesValues.put("$s_id", "BII-S-1");
	}

	public QueryBenchmarkMicroBenchmark(String[] sparqlQuery, String[] gremlinScript) {
		this.sparqlQuery = sparqlQuery;
		this.gremlinScript = gremlinScript;
	}

	@Parameters
	public static Collection<Object[]> data() {
		sparqlQueryObj = new SparqlQuery();
		gremlinQueryObj = new GremlinQuery();
		Object[][] data = new Object[][] {
				{ new String[] { sparqlQueryObj.BROWSE_QUERY },
						new String[] { gremlinQueryObj.BROWSE_QUERY } },
				{ new String[] { sparqlQueryObj.INVESTIGATION_QUERY },
						new String[] { gremlinQueryObj.INVESTIGATION_QUERY } },
				{
						new String[] { sparqlQueryObj.STUDY_QUERY,
								sparqlQueryObj.STUDY_CONTACTS_QUERY,
								sparqlQueryObj.STUDY_GROUPS_COUNT },
						new String[] { gremlinQueryObj.STUDY_QUERY,
								gremlinQueryObj.STUDY_CONTACTS_QUERY,
								gremlinQueryObj.STUDY_GROUPS_COUNT }, },
				{
						new String[] { sparqlQueryObj.MATERIAL_DERIVED_FROM_SOURCE, },
						new String[] { gremlinQueryObj.MATERIAL_DERIVED_FROM_SOURCE, }, }, };
		return Arrays.asList(data);
	}

	@Test
	public void testQuery() throws IllegalArgumentException,
			IllegalStateException, Exception {

		final HashMap<String, String> parsedScripts = new HashMap<String, String>();
		final HashMap<String, String> parsedQueries = new HashMap<String, String>();
		FileWriter fw = new FileWriter(new File(getClass().getResource(
				"/results.txt").getFile()),true);
		fw.append("Neo4J Gremlin Duration " + " "
				);
		fw.close();
		fw = new FileWriter(new File(getClass().getResource(
				"/results.txt").getFile()),true);
		fw.append("Neo4J Gremlin Duration " + " "
				);
		fw.close();
		String gremlinScriptNames = "";
		String sparqlScriptNames = "";

		for (String script : gremlinScript) {
			String parsedScript = gremlinQueryObj.parseFilesToString(
					gremlinQueryObj.PREFIX, script);
			for (String variable : variablesValues.keySet())
				parsedScript = parsedScript.replace(variable,
						variablesValues.get(variable));
			String scriptName = script.substring(script.lastIndexOf("/") + 1);
			gremlinScriptNames += scriptName;
			parsedScripts.put(scriptName, parsedScript);
		}

		final SPARQLParser parser = new SPARQLParser();
		for (String query : sparqlQuery) {
			String stringQuery = sparqlQueryObj.parseFilesToString(
					sparqlQueryObj.PREFIX, query);
			for (String variable : variablesValues.keySet())
				stringQuery = stringQuery.replace(variable,
						variablesValues.get(variable));
			String queryName = query.substring(query.lastIndexOf("/") + 1);
			sparqlScriptNames += queryName;
			parsedQueries.put(queryName, stringQuery);
		}

		/**
		 * Neo4J Performance
		 */
		Callable callableGremlin = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedScripts.entrySet()) {
					QueryResultsIterator innerResult = new QueryResultsIterator(
							(GremlinGroovyPipeline) neo4jEngine.eval(entry
									.getValue()));

					try {
						while (innerResult.hasNext()) {
							try {
								innerResult.next();
							} catch (QueryEvaluationException e) {
								e.printStackTrace();
							}
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		Callable callableSparql = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedQueries.entrySet()) {
					ParsedQuery parsedQuery = parser.parseQuery(
							entry.getValue(), baseURI);
					QueryResultsIterator innerResult = new QueryResultsIterator(
							neo4jSailConnection.evaluate(
									parsedQuery.getTupleExpr(),
									parsedQuery.getDataset(),
									new EmptyBindingSet(), false));
					try {
						while (innerResult.hasNext()) {
							innerResult.next();
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		Benchmark gremlinBenchmark = new Benchmark(callableGremlin);
		fw = new FileWriter(new File(getClass().getResource(
				"/results.txt").getFile()),true);
		fw.append("Neo4J Gremlin Duration " + gremlinScriptNames + " "
				+ gremlinBenchmark.toString());
		Benchmark sparqlBenchmark = new Benchmark(callableSparql);
		fw.append("Neo4J Sparql Duration " + sparqlScriptNames + " "
				+ sparqlBenchmark.toString());
		
		fw.close();

		/**
		 * Tinker Graph evalution
		 */

		callableGremlin = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedScripts.entrySet()) {
					QueryResultsIterator innerResult = new QueryResultsIterator(
							(GremlinGroovyPipeline) tinkerEngine.eval(entry
									.getValue()));

					try {
						while (innerResult.hasNext()) {
							try {
								innerResult.next();
							} catch (QueryEvaluationException e) {
								e.printStackTrace();
							}
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		callableSparql = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedQueries.entrySet()) {
					ParsedQuery parsedQuery = parser.parseQuery(
							entry.getValue(), baseURI);
					QueryResultsIterator innerResult = new QueryResultsIterator(
							tinkerSailConnection.evaluate(
									parsedQuery.getTupleExpr(),
									parsedQuery.getDataset(),
									new EmptyBindingSet(), false));
					try {
						while (innerResult.hasNext()) {
							innerResult.next();
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		gremlinBenchmark = new Benchmark(callableGremlin);
		fw = new FileWriter(new File(getClass().getResource("/results.txt")
				.getFile()),true);
		fw.append("TinkerGraph Gremlin Duration " + gremlinScriptNames + " "
				+ gremlinBenchmark.toString());
		sparqlBenchmark = new Benchmark(callableSparql);
		fw.append("TinkerGraph Sparql Duration " + sparqlScriptNames + " "
				+ sparqlBenchmark.toString());
		
		fw.close();

		/**
		 * Orient Graph evalution
		 */

		callableGremlin = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedScripts.entrySet()) {
					QueryResultsIterator innerResult = new QueryResultsIterator(
							(GremlinGroovyPipeline) orientEngine.eval(entry
									.getValue()));

					try {
						while (innerResult.hasNext()) {
							try {
								innerResult.next();
							} catch (QueryEvaluationException e) {
								e.printStackTrace();
							}
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		callableSparql = new Callable() {
			public String call() throws Exception {
				for (Entry<String, String> entry : parsedQueries.entrySet()) {
					ParsedQuery parsedQuery = parser.parseQuery(
							entry.getValue(), baseURI);
					QueryResultsIterator innerResult = new QueryResultsIterator(
							orientSailConnection.evaluate(
									parsedQuery.getTupleExpr(),
									parsedQuery.getDataset(),
									new EmptyBindingSet(), false));
					try {
						while (innerResult.hasNext()) {
							innerResult.next();
						}
					} catch (QueryEvaluationException e) {
						e.printStackTrace();
					}
				}
				return null;
			}
		};

		gremlinBenchmark = new Benchmark(callableGremlin);
		fw = new FileWriter(new File(getClass().getResource("/results.txt")
				.getFile()),true);
		fw.append("OrientDB Gremlin Duration " + gremlinScriptNames + " "
				+ gremlinBenchmark.toString());
		sparqlBenchmark = new Benchmark(callableSparql);
		fw.append("OrientDB Sparql Duration " + sparqlScriptNames + " "
				+ sparqlBenchmark.toString());
		
		fw.close();

	}
}