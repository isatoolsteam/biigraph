package org.isatools.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class StudyGroup {
	
	private JsonObject studyGroup;
//	private HashMap<String, HashSet<String>> characteristicsMap;
//	private JsonArray characteristics;
	private JsonArray samples;
	
	public StudyGroup() {
		studyGroup=new JsonObject();
		samples=new JsonArray();
//		characteristics=new JsonArray();
		studyGroup.add("samples",samples);
//		sampleGroup.add("characteristics",characteristics);
//		characteristicsMap= new HashMap<String, HashSet<String>>();
	}
	
	public void add(String property, JsonArray value) {
		studyGroup.add(property, value);
	}

	public void addProperty(String name, String value) {
		studyGroup.addProperty(name, value);
	}

	public JsonElement get(String name) {
		return studyGroup.get(name);
	}

	public void addSample(JsonObject sampleObj) {
		samples.add(sampleObj);
	}

	public JsonElement toJsonObject() {
		return studyGroup;
	}

//	public void addCharacteristic(String characteristic_type, String characteristic) {
//		HashSet<String> set = characteristicsMap.get(characteristic_type);
//		if(set==null){
//			set=new HashSet<String>();
//			characteristicsMap.put(characteristic_type, set);
//		}
//		set.add(characteristic);
//	}

//	public JsonElement toJsonObject() {
//		for (Entry<String,HashSet<String>> entry : characteristicsMap.entrySet()) {
//			JsonObject characteristic = new JsonObject();
//			characteristic.addProperty("type", entry.getKey());
//			JsonArray els = new JsonArray();
//			characteristic.add("characteristics", els);
//			for (String el : entry.getValue()) {
//				els.add(new JsonPrimitive(el));
//			}
//			characteristics.add(characteristic);
//		}
//		return sampleGroup;
//	}

//	public Set<String> getCharacteristicTypes() {
//		return characteristicsMap.keySet();
//	}
//
//	public Collection<? extends String> getCharacteristicValues(String type) {
//		return characteristicsMap.get(type);
//	}

}
