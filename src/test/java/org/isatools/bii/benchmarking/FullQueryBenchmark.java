package org.isatools.bii.benchmarking;

import static org.junit.Assert.assertEquals;

import org.isatools.database.GraphDatabase;
import org.isatools.database.GremlinQuery;
import org.isatools.database.SparqlQuery;
import org.junit.Test;

import com.google.gson.JsonObject;

public class FullQueryBenchmark {
	
	@Test
	public void getStudyTest() throws ClassNotFoundException{
//		Class.forName(GraphDatabase.class.getName());
		long startTime=0,total=0;
		startTime=System.currentTimeMillis();
		JsonObject out1 = new SparqlQuery().getStudyInfo("BII-S-1");
		total=System.currentTimeMillis()-startTime;
		System.out.println("SPARQL Study Query time "+total+" ms");
		startTime=System.currentTimeMillis();
		JsonObject out2 = new GremlinQuery().getStudyInfo("BII-S-1");
		total=System.currentTimeMillis()-startTime;
		System.out.println("GREMLIN Study Query time "+total+" ms");
		assertEquals(out1.toString().length(),out2.toString().length());
	}
	
	@Test
	public void getInvestigationTest() throws ClassNotFoundException{
		Class.forName(GraphDatabase.class.getName());
		long startTime=0,total=0;
		startTime=System.currentTimeMillis();
		JsonObject out1 = new SparqlQuery().getInvestigationInfo("BII-I-1");
		total=System.currentTimeMillis()-startTime;
		System.out.println("SPARQL Investigation Query time "+total+" ms");
		startTime=System.currentTimeMillis();
		JsonObject out2 = new GremlinQuery().getInvestigationInfo("BII-I-1");
		total=System.currentTimeMillis()-startTime;
		System.out.println("GREMLIN Investigation Query time "+total+" ms");
		assertEquals(out1.toString().length(),out2.toString().length());
	}
	
	@Test
	public void getBrowseTest() throws ClassNotFoundException{
		Class.forName(GraphDatabase.class.getName());
		long startTime=0,total=0;
		startTime=System.currentTimeMillis();
		JsonObject out1 = new SparqlQuery().getBrowseInfo();
		total=System.currentTimeMillis()-startTime;
		System.out.println("SPARQL Browse Query time "+total+" ms");
		startTime=System.currentTimeMillis();
		JsonObject out2 = new GremlinQuery().getBrowseInfo();
		total=System.currentTimeMillis()-startTime;
		System.out.println("GREMLIN Browse Query time "+total+" ms");
		assertEquals(out1.toString().length(),out2.toString().length());
	}
	
	@Test
	public void getAssayTest() throws ClassNotFoundException{
		Class.forName(GraphDatabase.class.getName());
		long startTime=0,total=0;
		startTime=System.currentTimeMillis();
		JsonObject out1 = new SparqlQuery().getAssayTypeDetails("BII-S-1", "OBI:metabolite profiling", "OBI:mass spectrometry");
		System.out.println(out1);
		total=System.currentTimeMillis()-startTime;
		System.out.println("SPARQL Browse Query time "+total+" ms");
//		startTime=System.currentTimeMillis();
//		JsonObject out2 = new GremlinQuery().getBrowseInfo();
//		total=System.currentTimeMillis()-startTime;
//		System.out.println("GREMLIN Browse Query time "+total+" ms");
//		assertEquals(out1.toString().length(),out2.toString().length());
	}
	
}
