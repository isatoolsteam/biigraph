package org.isatools.bii.loader;

import info.aduna.iteration.CloseableIteration;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Statement;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;


/**
 * Created with IntelliJ IDEA.
 * User: eamonnmaguire
 * Date: 29/05/2013
 * Time: 15:08
 * To change this template use File | Settings | File Templates.
 */
public class ISARDFTest {

    private GraphSail<TinkerGraph> sail;
    private TinkerGraph graph;
    private SailConnection sailConnection;
    private SailGraph sailGraph;

    @Before
    public void testLoading() throws SailException, FileNotFoundException {
        graph = new TinkerGraph();
        sail = new GraphSail<TinkerGraph>(graph);
        sailGraph = new SailGraph(sail);
        sailConnection = sail.getConnection();

        sailGraph.loadRDF(ISARDFTest.class.getResourceAsStream("/data/faahko.rdf"), "http://isa-tools.org/rdf#", "rdf-xml", null);
    }

    @Test
    public void testGetStatements() throws SailException {
        System.out.println("____ START testGetStatements() _____");
        System.out.println("get statements: ?s ?p ?o ?g");
        CloseableIteration<? extends Statement, SailException> results = sailConnection.getStatements(null, null, null, false);
        while (results.hasNext()) {
            System.out.println(results.next());
        }

        results = sailConnection.getStatements(RDF.TYPE, null, null, false);
        while (results.hasNext()) {
            System.out.println(results.next());
        }
        System.out.println("____ END testGetStatements() _____");
    }

    @Test
    public void testSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();
        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?x ?y WHERE { ?x <" + RDF.TYPE + "> ?y }";
        ParsedQuery query = parser.parseQuery(queryString, "http://isa-tools.org/rdf#");

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

    @Test
    public void testBlueprints(){
        Graph graph = ((GraphSail) sail).getBaseGraph();
        System.out.println();
        System.out.println("------- Vertices ---------");
        for (Vertex v : graph.getVertices()) {
            System.out.println("------");
            System.out.println(v);
            for (String key : v.getPropertyKeys()) {
                System.out.println(key + "=" + v.getProperty(key));
            }
        }
        System.out.println("------- Edges ---------");
        for (Edge e : graph.getEdges()) {
            System.out.println("------");
            System.out.println(e);
            for (String key : e.getPropertyKeys()) {
                System.out.println(key + "=" + e.getProperty(key));
            }
        }
    }


}
