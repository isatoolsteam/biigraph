package org.isatools.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

public class Study {
	private JsonObject study;
	private JsonArray s_organisms;
	private JsonArray s_assays;

	public Study() {
		study = new JsonObject();
		s_organisms = new JsonArray();
		s_assays = new JsonArray();
		study.add("s_organisms", s_organisms);
		study.add("s_assays", s_assays);

	}

	public void add(String property, JsonElement value) {
		study.add(property, value);
	}

	public void addProperty(String name, String value) {
		study.addProperty(name, value);
	}

	public boolean has(String name) {
		return study.has(name);
	}

	
	public JsonArray getAssays() {
		return s_assays;
	}

	public void setAssays(JsonArray s_assays) {
		this.s_assays.addAll(s_assays);
	}

	public JsonObject getStudy() {
		return study;
	}

	public JsonArray getOrganisms() {
		return s_organisms;
	}
	
	public void addOrganism(JsonPrimitive organism){
		for (JsonElement organism_el : s_organisms) {
			if(organism_el.getAsString().equals(organism.getAsString())){
				return;
			}
		}
		s_organisms.add(organism);
	}

	public void addAssay(JsonObject assay) {
		s_assays.add(assay);
	}

}