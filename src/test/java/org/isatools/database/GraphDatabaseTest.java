package org.isatools.database;

import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.JsonObject;

public class GraphDatabaseTest {
	
	private static SparqlQuery query;

	@BeforeClass
	public static void init(){
		query=new SparqlQuery();
	}

	@Test
	public void testDeleteInvestigation() throws ClassNotFoundException {
		Class.forName(GraphDatabase.class.getName());
		query.deleteInvestigation("BII-I-1");
		JsonObject info = query.getInvestigationInfo("BII-I-1");
		System.out.println(info);
		info = query.getStudyBrowseView("BII-I-1");
		System.out.println(info);
		info = query.getStudyInfo("BII-I-1");
		System.out.println(info);
		info = query.getBrowseInfo();
		System.out.println(info);
	}
	
	@Test
	public void testDeleteStudy() throws ClassNotFoundException {
		Class.forName(GraphDatabase.class.getName());
		query.deleteStudy("BII-S-1",false);
		JsonObject info = query.getStudyInfo("BII-S-1");
		System.out.println(info);
		info = query.getBrowseInfo();
		System.out.println(info);
	}
	
	@Test
	public void testInvestigation() throws ClassNotFoundException {
		Class.forName(GraphDatabase.class.getName());
		JsonObject json = query.getInvestigationInfo("BII-I-1");
		assertTrue(json.has("i_id"));
		assertTrue(json.has("i_studies"));
		assertTrue(json.has("i_publications"));
		assertTrue(json.has("i_description"));
		assertTrue(json.has("i_title"));
	}
	
	@Test
	public void testBrowse() throws ClassNotFoundException {
		Class.forName(GraphDatabase.class.getName());
		JsonObject json = query.getBrowseInfo();
		assertTrue(json.has("investigations"));
		if(json.get("investigations").getAsJsonArray().size()>0){
			JsonObject investigation = json.get("investigations").getAsJsonArray().get(0).getAsJsonObject();
			assertTrue(investigation.has("i_id"));
			assertTrue(investigation.has("i_studies"));
		}
		assertTrue(json.has("studies"));
		if(json.get("studies").getAsJsonArray().size()>0){
			JsonObject study = json.get("studies").getAsJsonArray().get(0).getAsJsonObject();
			assertTrue(study.has("s_id"));
			assertTrue(study.has("s_organisms"));
			assertTrue(study.has("s_title"));
		}
	}
	
	@Test
	public void testStudy() throws ClassNotFoundException {
		Class.forName(GraphDatabase.class.getName());
		JsonObject json = query.getStudyInfo("BII-S-1");
		assertTrue(json.has("s_id"));
		assertTrue(json.has("s_publications"));
		assertTrue(json.has("s_description"));
		assertTrue(json.has("s_title"));
	}

}
