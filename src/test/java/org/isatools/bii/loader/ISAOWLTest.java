package org.isatools.bii.loader;

import info.aduna.iteration.CloseableIteration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.isatools.owl.BFO;
import org.isatools.owl.IAO;
import org.isatools.owl.OBI;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

/**
 * Created by the ISATeam.
 * User: agbeltran
 * Date: 27/06/2013
 * Time: 16:48
 *
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra Gonzalez-Beltran</a>
 */
public class ISAOWLTest {

    private GraphSail<TinkerGraph> sail;
    private TinkerGraph graph;
    private SailConnection sailConnection;
    private SailGraph sailGraph;
    private String baseURI;


    @Before
    public void testLoading() throws SailException, FileNotFoundException, MalformedURLException, IOException {
        graph = new TinkerGraph();
        sail = new GraphSail<TinkerGraph>(graph);

        sailGraph = new SailGraph(sail);

//        Sail reasoner = new ForwardChainingRDFSInferencer(new GraphSail<TinkerGraph>(graph));
//        reasoner.initialize();

        sailConnection = sail.getConnection();

//        CloseableIteration<? extends Namespace, SailException> namespaces =  sailConnection.getNamespaces();
//        System.out.println("Namespaces...");
//        while(namespaces.hasNext()){
//            System.out.println(namespaces.next());
//        }
//        System.out.println("...end of Namespaces.");

        baseURI = "http://isa-tools.org/isa/";


        sailGraph.loadRDF(getInputStream(new URL("http://purl.obolibrary.org/obo/obi.owl")), "http://purl.obolibrary.org/obo/OBI_", "rdf-xml", null);

        sailGraph.loadRDF(ISAOWLTest.class.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"), baseURI, "rdf-xml", null);
        sailGraph.loadRDF(ISAOWLTest.class.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml", null);
        sailGraph.loadRDF(ISAOWLTest.class.getResourceAsStream("/data/faahko.owl"), baseURI, "rdf-xml", null);


//        Sail reasoner = new ForwardChainingRDFSInferencer(sailGraph);
//        reasoner.initialize();

        sailGraph.addNamespace("obi", "http://purl.obolibrary.org/obo/OBI_");

//        Map<String, String> namespacesGraph = sailGraph.getNamespaces();
//        for(String key: namespacesGraph.keySet()){
//            System.out.println("key="+key+" , value="+namespacesGraph.get(key));
//        }
//        System.out.println("after printing namespaces...");

    }


    //@Test
    public void testSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();
        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?x ?y WHERE { ?x <" + RDF.TYPE + "> ?y }";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }


//    @Test
    public void testGetPlannedProcessesSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?label WHERE { ?x <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/OBI_0000011>. ?x <"+ RDFS.LABEL +"> ?label . }";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }


    /**
     *
     * investigation identifier
     * studies identifiers
     * study title
     * organism
     * assay measurement + technology + number of assays
     *
     *
     */
//    @Test
    public void testGetBrowseInfo() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testGetStudiesSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;

        String queryString = "SELECT DISTINCT ?investigation_identifier ?study_identifier ?study_title ?organism ?organism_iri " +
                "WHERE { " +
                "?study <" + RDF.TYPE + "> <"+ OBI.INVESTIGATION+">. " +
                "?study <"+ RDFS.LABEL +"> ?study_identifier. " +
                "?study_title_iri <"+RDF.TYPE+"> <http://purl.obolibrary.org/obo/OBI_0001622>. " +
                "?study_title_iri <"+ IAO.DENOTES +"> ?study. " +
                "?study_title_iri <http://isa-tools.org/isa/ISA_0000144> ?study_title. " +
                " OPTIONAL { " +
                "?study <"+ BFO.IS_PART_OF +"> ?investigation. " +
                "?investigation <"+ RDF.TYPE +"> <http://purl.obolibrary.org/obo/OBI_0000011>. " +
                "?investigation <"+ RDFS.LABEL +"> ?investigation_identifier. " +
                "  } "+
                " OPTIONAL { " +
                "?source <"+ RDF.TYPE +"> <http://purl.obolibrary.org/obo/BFO_0000040>. " +
                "?source <http://purl.obolibrary.org/obo/OBI_0000295> ?study. " +
                "?source <"+ RDF.TYPE + "> ?organism_iri. "+
                "?organism_iri <"+ RDF.TYPE +"> <"+OBI.ORGANISM+">. " +
                "?organism_iri <"+ RDFS.LABEL +"> ?organism." +
                "  } "+
                " OPTIONAL { " +
                "?source <"+ RDF.TYPE +"> <http://purl.obolibrary.org/obo/BFO_0000040>. " +
                "?source <http://purl.obolibrary.org/obo/OBI_0000295> ?study. " +
                "?source <http://purl.obolibrary.org/obo/BFO_0000053> ?characteristic. "+
                "?characteristic <"+ RDF.TYPE +"> <http://purl.obolibrary.org/obo/BFO_0000005>. " +
                "?characteristic <"+ RDFS.COMMENT +"> ?comment. " +
                "?characteristic <"+ RDFS.LABEL +"> ?organism. " +
                "FILTER regex(?comment, \"organism\")" +
                "  } "+
                "}";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

    /**
     * Given the investigation identifier,
     * retrieve the associated:
     *    - investigation title
     *    - investigation description
     *    - publications
     *    - contacts
     *
     * @throws SailException
     * @throws MalformedQueryException
     * @throws QueryEvaluationException
     */
   @Test
    public void testGetInvestigationInfo() throws SailException, MalformedQueryException, QueryEvaluationException {
    	System.out.println("____ START testGetInvestigationInfo() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
    	
    	String queryString = "SELECT DISTINCT ?investigation_id ?investigation_title " +
    			"?pub_title ?pubmed_id ?pub_author_list ?pub_doi ?person "+
    			"?person_last_name ?person_first_name ?person_mid_initials ?person_email "+
    			"?person_phone ?person_fax ?person_address ?person_affiliation ?person_roles "+
                "WHERE { " +
                "?investigation <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/OBI_0000011>. "+
                "?investigation_id_iri <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/IAO_0000577>. "+
                "?investigation_id_iri <"+ IAO.DENOTES +"> ?investigation. " +
                "?investigation_id_iri <"+ RDFS.LABEL +"> ?investigation_id. "+
                "?investigation_title_iri <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/IAO_0000300>. "+
                "?investigation_title_iri <"+ IAO.DENOTES +"> ?investigation. " +
                "?investigation_title_iri <http://isa-tools.org/isa/ISA_0000144> ?investigation_title. " +
                //Publications
                " OPTIONAL { " +
                "?pub <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/IAO_0000311>. "+
                "?pub <"+ IAO.DENOTES +"> ?investigation. " +
                "?pub_title_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000305>. " +
                "?pub_title_iri <" + IAO.DENOTES + ">  ?pub. " +
                "?pub_title_iri <" + RDFS.LABEL + ">  ?pub_title. " +
                "?pubmed_id_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/OBI_0001617>. " +
                "?pubmed_id_iri <" + IAO.DENOTES + ">  ?pub. " +
                "?pubmed_id_iri <" + RDFS.LABEL + ">  ?pubmed_id. " +
                "?pub_author_list_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000321>. " +
                "?pub_author_list_iri <http://purl.obolibrary.org/obo/BFO_0000050>  ?pub. " +
                "?pub_author_list_iri <" + RDFS.LABEL + ">  ?pub_author_list. " +
                "?pub_doi_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000577>. " +
                "?pub_doi_iri <" + IAO.DENOTES + ">  ?pub. " +
                "?pub_doi_iri <" + RDFS.LABEL + ">  ?pub_doi. " +
                "  } "+
                //Contacts
                " OPTIONAL { " +
                "?person <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/NCBITaxon_9606>. "+
                "?person <http://purl.obolibrary.org/obo/BFO_0000056> ?investigation. " +
                " OPTIONAL { " +
                "?person_last_name_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000590>. " +
                "?person_last_name_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_last_name_iri <" + RDFS.LABEL + ">  ?person_last_name. " +
                "  } "+
                " OPTIONAL { " +
                "?person_first_name_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000590>. " +
                "?person_first_name_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_first_name_iri <" + RDFS.LABEL + ">  ?person_first_name. " +
                "  } "+
                " OPTIONAL { " +
                "?person_mid_initials_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000590>. " +
                "?person_mid_initials_iri <http://purl.obolibrary.org/obo/BFO_0000050>  ?person. " +
                "?person_mid_initials_iri <" + RDFS.LABEL + ">  ?person_mid_initials. " +
                "  } "+
                " OPTIONAL { " +

                "?person_email_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000429>. " +
                "?person_email_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_email_iri <" + RDFS.LABEL + ">  ?person_email. " +
                "  } "+
                " OPTIONAL { " +

                "?person_phone_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000030>. " +
                "?person_phone_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_phone_iri <" + RDFS.LABEL + ">  ?person_phone. " +
                "  } "+
                " OPTIONAL { " +

                "?person_fax_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000030>. " +
                "?person_fax_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_fax_iri <" + RDFS.LABEL + ">  ?person_fax. " +
                "  } "+
                " OPTIONAL { " +

                "?person_address_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/IAO_0000030>. " +
                "?person_address_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_address_iri <" + RDFS.LABEL + ">  ?person_address. " +
                "  } "+
                " OPTIONAL { " +

                "?person_affiliation_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/OBI_0000245>. " +
                "?person_affiliation_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_affiliation_iri <" + RDFS.LABEL + ">  ?person_affiliation. " +
                "  } "+
                " OPTIONAL { " +

                "?person_roles_iri <" + RDF.TYPE + ">  <http://purl.obolibrary.org/obo/OBI_0000202>. " +
                "?person_roles_iri <" + IAO.DENOTES + ">  ?person. " +
                "?person_roles_iri <" + RDFS.LABEL + ">  ?person_roles. " +
                "  } "+
                "  } "+
                //"  } "+
                "FILTER (?investigation_id = \"BII-I-1\")" +
                "}";
    	ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

//  @Test
  public void testGetPersons() throws SailException, MalformedQueryException, QueryEvaluationException {
	    	System.out.println("____ START testGetPersons() _____");
	        SPARQLParser parser = new SPARQLParser();

	        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
	    	
	        String queryString = "SELECT DISTINCT ?person " +
	                "WHERE { " +
	                "?investigation <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/OBI_0000011>. "+
	                "?person <" + RDF.TYPE + "> <http://purl.obolibrary.org/obo/NCBITaxon_9606>. "+
	                " OPTIONAL { " +
	                "?person <http://purl.obolibrary.org/obo/BFO_0000056> ?investigation. " +
	                "}"+
	                "}";
	    	ParsedQuery query = parser.parseQuery(queryString, baseURI);

	        System.out.println("\nSPARQL: " + queryString);
	        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
	        while (sparqlResults.hasNext()) {
	            System.out.println(sparqlResults.next());
	        }

	        System.out.println("____ END testSPARQLQuery() _____");
	    }
    

//    @Test
    public void testGetStudiesSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testGetStudiesSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?study_label ?desc_value " +
                "WHERE { " +
                "?study <" + RDF.TYPE + "> <"+ OBI.INVESTIGATION+">. " +
                "?study <"+ RDFS.LABEL +"> ?study_label ." +
                "?description <"+ RDF.TYPE + "> <" + OBI.INVESTIGATION_DESCRIPTION + ">." +
                "?description <"+ IAO.DENOTES +"> ?study." +
                "?description <http://isa-tools.org/isa/ISA_0000144> ?desc_value ." +
                "}";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

//    @Test
    public void testGetAssaysSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testGetStudiesSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?assay_label ?study_label " +
                "WHERE { " +
                "?study <" + RDF.TYPE + "> <"+ OBI.INVESTIGATION+">. " +
                "?assay <"+ BFO.IS_PART_OF +"> ?study ." +
                "?assay <"+ RDF.TYPE + "> <" + OBI.ANALYTE_ASSAY + ">." +
                "?assay <"+ RDFS.LABEL +"> ?assay_label." +
                "}";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

//    @Test
    public void testGetAssaysCountsQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testGetStudiesSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();

        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?assay_label ?study_label count " +
                "WHERE { " +
                "?study <" + RDF.TYPE + "> <"+ OBI.INVESTIGATION+">. " +
                "?assay <"+ BFO.IS_PART_OF +"> ?study ." +
                "?assay <"+ RDF.TYPE + "> <" + OBI.ANALYTE_ASSAY + ">." +
                "?assay <"+ RDFS.LABEL +"> ?assay_label." +
                "}";
        ParsedQuery query = parser.parseQuery(queryString, baseURI);

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

    public static InputStream getInputStream(URL url) throws IOException {
        if(url.getProtocol().equals("http")) {
            URLConnection conn;
            conn = url.openConnection();
            conn.setRequestProperty("Accept", "application/rdf+xml");
            conn.addRequestProperty("Accept", "text/xml");
            conn.addRequestProperty("Accept", "*/*");
            return conn.getInputStream();
        }
        else {
            return url.openStream();
        }
    }


}
