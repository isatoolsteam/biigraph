package org.isatools.database.structures;

public class QueryBinding {
		private String value;

		public QueryBinding() {
		}

		public QueryBinding(org.openrdf.query.Binding binding) {
			value = binding.getValue().stringValue();

		}

		public QueryBinding(Object column) {
			if (column == null)
				value = null;
			else
				value = column.toString();
		}

		public String getValue() {
			return value;
		}
	}