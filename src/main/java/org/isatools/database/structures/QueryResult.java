package org.isatools.database.structures;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.openrdf.query.BindingSet;

import com.tinkerpop.pipes.util.structures.Row;

public class QueryResult {

	private BindingSet bindingSet;
	private Row row;
	private Map<String, String> map;

	public QueryResult(BindingSet result) {
		this.bindingSet = result;
	}

	public QueryResult(Object result) {
		if (result instanceof Map) {
			this.map= (Map<String, String>) result;
//			this.map = entry.getKey();
//			this.map.put("count", Long.toString(entry.getValue()));
//		if (result instanceof Entry) {
//			Entry<HashMap<String, String>, Long> entry = (Entry<HashMap<String, String>, Long>) result;
//			this.map = entry.getKey();
//			this.map.put("count", Long.toString(entry.getValue()));
		} else if (result instanceof Row) { 
			this.row = (Row) result;
		} 
	}

	public Set<String> getBindingNames() {
		if (bindingSet != null)
			return bindingSet.getBindingNames();
		if (map != null)
			return map.keySet();
		return new HashSet(row.getColumnNames());
	}

	public QueryBinding getBinding(String name) {
		if (bindingSet != null)
			return new QueryBinding(bindingSet.getBinding(name));
		if (map != null)
			return new QueryBinding(map.get(name));
		return new QueryBinding(row.getColumn(name));
	}

	@Override
	public String toString() {
		if (bindingSet != null)
			return bindingSet.toString();
		if (map != null)
			return map.toString();
		return row.toString();
	}

}
