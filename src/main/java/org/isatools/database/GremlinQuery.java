package org.isatools.database;

import org.isatools.database.structures.QueryResultsIterator;

import com.tinkerpop.gremlin.groovy.GremlinGroovyPipeline;

public class GremlinQuery extends Query {

	
	public GremlinQuery() {
		super("/database/queries/gremlin_scripts/", ".grm");
	}

	@Override
	public QueryResultsIterator runQuery(String queryString) {
			Object results = null;
			try {
				results=GraphDatabase.evalGremlin(queryString);
			} finally {
			}
			return new QueryResultsIterator((GremlinGroovyPipeline) results);
	}


	@Override
	public void runUpdateQuery(String queryString) {

	}

}
