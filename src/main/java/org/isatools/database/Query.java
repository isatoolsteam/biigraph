package org.isatools.database;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.isatools.database.structures.QueryBinding;
import org.isatools.database.structures.QueryResult;
import org.isatools.database.structures.QueryResultsIterator;
import org.isatools.parser.QuerytoJSON;
import org.openrdf.query.QueryEvaluationException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Abstract class with the common elements for SparqlQuery and GremlinQuery.
 */
public abstract class Query {

	protected String baseURI = "http://isa-tools.org/isa/";
	private static JsonObject IRI_MAPPING;
	public String BROWSE_QUERY_PATH;
	public String STUDY_BROWSE_VIEW;
	public String INVESTIGATION_QUERY_PATH;
	public String STUDY_QUERY_PATH;
	public String ASSAY_QUERY_PATH;
	public String BROWSE_QUERY;
    public String SAMPLE_QUERY;
	public String INVESTIGATION_QUERY;
	public String INVESTIGATION_CONTACTS_QUERY;
	public String INVESTIGATION_ID_FROM_STUDY_ID;
	public String INVESTIGATION_STUDIES_IDS;
	public String STUDY_CONTACTS_QUERY;
	public String STUDY_QUERY;
	public String STUDY_GROUP_MEMBERS;
	public String STUDY_ASSAY_QUERY;
	public String ASSAY_QUERY;
	public String UPDATE_INVESTIGATION_QUERY;
	public String PREFIX;
	public String UPDATE_STUDY_QUERY;
	public String UPDATE_STUDY_ID_QUERY;
	public String UPDATE_INVESTIGATION_ID_QUERY;
	public String UPDATE_PUBLICATION_QUERY;
	public String STUDY_ASSAYS;
	public String STUDY_GROUPS_COUNT;
	public String DELETE_INVESTIGATION_QUERY;
	public String DELETE_INVESTIGATION_CONTACTS_QUERY;
	public String DELETE_STUDY_QUERY;
	public String DELETE_STUDY_CONTACTS_QUERY;
	public String INVESTIGATION_ID;
	public String INVESTIGATION_ID_URI;
	public String STUDY_ID_URI;
	public String STUDY_ID;
	public String MATERIAL_DERIVED_FROM_SOURCE;
    public String SAMPLE_QUERY_PATH;

	static {
		String str = "";
		BufferedReader reader = null;
		try {

			InputStreamReader is = new InputStreamReader(
					Query.class.getResourceAsStream("/database/queries/"
							+ "mapping_iri.json"));
			reader = new BufferedReader(is);
			String line = "";
			while ((line = reader.readLine()) != null) {
				str += line;
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		JsonParser parser = new JsonParser();
		IRI_MAPPING = (JsonObject) parser.parse(str);
	}

	public Query(String QUERY_PATH, String EXTENSION) {
		BROWSE_QUERY_PATH = QUERY_PATH + "browse/";
        SAMPLE_QUERY_PATH = QUERY_PATH + "sample/";
		INVESTIGATION_QUERY_PATH = QUERY_PATH + "investigation/";
		STUDY_QUERY_PATH = QUERY_PATH + "study/";
		ASSAY_QUERY_PATH = QUERY_PATH + "assay/";
		BROWSE_QUERY = BROWSE_QUERY_PATH + "browse" + EXTENSION;
        SAMPLE_QUERY = SAMPLE_QUERY_PATH + "sample" + EXTENSION;
		INVESTIGATION_QUERY = INVESTIGATION_QUERY_PATH + "investigation"
				+ EXTENSION;
		INVESTIGATION_CONTACTS_QUERY = INVESTIGATION_QUERY_PATH
				+ "investigation_contacts" + EXTENSION;
		STUDY_CONTACTS_QUERY = STUDY_QUERY_PATH + "study_contacts" + EXTENSION;
		STUDY_QUERY = STUDY_QUERY_PATH + "study" + EXTENSION;
		STUDY_ASSAYS = STUDY_QUERY_PATH + "study_assays" + EXTENSION;
		STUDY_GROUPS_COUNT = STUDY_QUERY_PATH + "study_groups_count"
				+ EXTENSION;
		STUDY_GROUP_MEMBERS = STUDY_QUERY_PATH + "study_group_members"
				+ EXTENSION;
		STUDY_ASSAY_QUERY = BROWSE_QUERY_PATH + "assay_types_count_per_study"
				+ EXTENSION;
		STUDY_BROWSE_VIEW = STUDY_QUERY_PATH+ "study_browse_view" + EXTENSION;
		INVESTIGATION_ID_FROM_STUDY_ID = INVESTIGATION_QUERY_PATH
				+ "investigation_id_from_study_id" + EXTENSION;
		INVESTIGATION_STUDIES_IDS = INVESTIGATION_QUERY_PATH
				+ "investigation_studies_ids" + EXTENSION;
		ASSAY_QUERY = ASSAY_QUERY_PATH + "assay_type_details" + EXTENSION;
		UPDATE_INVESTIGATION_QUERY = INVESTIGATION_QUERY_PATH
				+ "update_investigation" + EXTENSION;
		PREFIX = QUERY_PATH + "prefix" + EXTENSION;
		UPDATE_STUDY_QUERY = STUDY_QUERY_PATH + "update_study" + EXTENSION;
		UPDATE_STUDY_ID_QUERY = STUDY_QUERY_PATH + "update_study_id"
				+ EXTENSION;
		UPDATE_INVESTIGATION_ID_QUERY = INVESTIGATION_QUERY_PATH
				+ "update_investigation_id" + EXTENSION;
		UPDATE_PUBLICATION_QUERY = QUERY_PATH + "update_publication"
				+ EXTENSION;
		DELETE_INVESTIGATION_QUERY = INVESTIGATION_QUERY_PATH
				+ "delete_investigation" + EXTENSION;
		DELETE_INVESTIGATION_CONTACTS_QUERY = INVESTIGATION_QUERY_PATH
				+ "delete_investigation_contacts" + EXTENSION;
		DELETE_STUDY_QUERY = STUDY_QUERY_PATH + "delete_study" + EXTENSION;
		DELETE_STUDY_CONTACTS_QUERY = STUDY_QUERY_PATH
				+ "delete_study_contacts" + EXTENSION;
		INVESTIGATION_ID = INVESTIGATION_QUERY_PATH + "investigation_id"
				+ EXTENSION;
		INVESTIGATION_ID_URI = INVESTIGATION_QUERY_PATH
				+ "investigation_id_uri" + EXTENSION;
		STUDY_ID = STUDY_QUERY_PATH + "study_id" + EXTENSION;
		STUDY_ID_URI = STUDY_QUERY_PATH + "study_id_uri" + EXTENSION;
		MATERIAL_DERIVED_FROM_SOURCE=QUERY_PATH+"material_derived_from_source"+EXTENSION;
	}

	public QueryResultsIterator runQuery(String prefixFile, String queryFile) {
		String queryString = parseFilesToString(prefixFile, queryFile);
		return runQuery(queryString);
	}

	//
	public QueryResultsIterator runQuery(String prefixFile, String queryFile,
			Map<String, String> variablesValues) {
		String queryString = parseFilesToString(prefixFile, queryFile);
		//
		for (String variable : variablesValues.keySet())
			queryString = queryString.replace(variable,
					variablesValues.get(variable));
		return runQuery(queryString);
	}

	public abstract QueryResultsIterator runQuery(String queryString);

	public abstract void runUpdateQuery(String queryString);

	public JsonObject getBrowseInfo() {
		long time = System.currentTimeMillis();
		QueryResultsIterator browseResults = runQuery(PREFIX, BROWSE_QUERY);
		QueryResultsIterator studyAssayResults = runQuery(PREFIX,
				STUDY_ASSAY_QUERY);
		JsonObject json = QuerytoJSON.parseBrowseResults(browseResults,
				studyAssayResults);
		System.out.println("Browse in " + (System.currentTimeMillis() - time));
		return json;
	}

	public JsonObject getInvestigationInfo(String investigationID) {
		long time = System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", investigationID);
		QueryResultsIterator invResults = runQuery(PREFIX, INVESTIGATION_QUERY,
				variablesValues);
		QueryResultsIterator invContactsResults = runQuery(PREFIX,
				INVESTIGATION_CONTACTS_QUERY, variablesValues);
		JsonObject json = QuerytoJSON.parseInvestigationResults(invResults,
				invContactsResults);
		System.out.println("Investigation " + investigationID + " in "
				+ (System.currentTimeMillis() - time));
		return json;
	}

	public JsonObject getStudyInfo(String studyID) {
		long time = System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		QueryResultsIterator studyResults = runQuery(PREFIX, STUDY_QUERY,
				variablesValues);
		QueryResultsIterator studyContactsResults = runQuery(PREFIX,
				STUDY_CONTACTS_QUERY, variablesValues);
		QueryResultsIterator studyGroupsResults = runQuery(PREFIX,
				STUDY_GROUPS_COUNT, variablesValues);
		System.out.println("Study Queries Duration " + studyID + " in "
				+ (System.currentTimeMillis() - time));
		
		long time2 = System.currentTimeMillis();
		JsonObject json = QuerytoJSON.parseStudyResults(studyResults,
				studyContactsResults, studyGroupsResults, this);
		System.out.println("Study Parsing Duration " + studyID + " in "
				+ (System.currentTimeMillis() - time2));
		System.out.println("Study Total " + studyID + " in "
				+ (System.currentTimeMillis() - time));
		return json;
	}

	public JsonObject getStudyBrowseView(String studyID) {
		long time = System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		QueryResultsIterator studyBrowseResults = runQuery(PREFIX,
				STUDY_BROWSE_VIEW, variablesValues);
		QueryResultsIterator studyAssayResults = runQuery(PREFIX,
				STUDY_ASSAYS, variablesValues);
		JsonObject json = QuerytoJSON
				.parseStudyBrowseViewResults(studyBrowseResults,studyAssayResults);
		System.out.println("GetBrowseStudyView " + studyID + " in "
				+ (System.currentTimeMillis() - time));
		return json;
	}

	public JsonObject getInvestigationStudiesBrowseView(String investigationID) {
		long time = System.currentTimeMillis();
		QueryResultsIterator studies = getInvestigationStudiesIDs(investigationID);
		JsonObject obj = null;
		try {
			if (!studies.hasNext())
				return null;
			
			JsonArray i_studies = new JsonArray();
			obj = new JsonObject();
			obj.add("i_studies", i_studies);
			while (studies.hasNext()) {
				QueryResult s_id = studies.next();
				String sid = s_id.getBinding("s_id").getValue();
				i_studies.add(getStudyBrowseView(sid));
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		System.out.println("getInvestigationStudiesBrowseView "
				+ investigationID + " in "
				+ (System.currentTimeMillis() - time));
		return obj;
	}

	public JsonObject getStudyAssayInfo(String studyID) {
		long time = System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		QueryResultsIterator studyAssaysResults = runQuery(PREFIX,
				STUDY_ASSAYS, variablesValues);
		JsonObject json = QuerytoJSON
				.parseStudyAssaysResults(studyAssaysResults);
		System.out.println("StudyAssayInfo " + studyID + " in "
				+ (System.currentTimeMillis() - time));
		return json;
	}

	public void deleteInvestigation(String investigationID) {
		long time = System.currentTimeMillis();
		QueryResultsIterator studies = getInvestigationStudiesIDs(investigationID);
		try {
			if (studies.hasNext()) {
				while (studies.hasNext()) {
					QueryResult s_id = studies.next();
					String sid = s_id.getBinding("s_id").getValue();
					deleteStudy(sid, true);
				}
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}

		String uri = getInvestigationID_Uri(investigationID);
		if (uri == null)
			return;
		GraphDatabase.removeVertex(uri);
		System.out.println("Delete Investigation " + investigationID + " in "
				+ (System.currentTimeMillis() - time));
	}

	private QueryResultsIterator getInvestigationStudiesIDs(
			String investigationID) {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", investigationID);
		return runQuery(PREFIX, INVESTIGATION_STUDIES_IDS, variablesValues);
	}

	public JsonObject getAssayTypeDetails(String studyID, String measurement,
			String technology) {
		long time = System.currentTimeMillis();
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		variablesValues.put("$measurement", measurement);
		variablesValues.put("$technology", technology);
		QueryResultsIterator results = runQuery(PREFIX, ASSAY_QUERY,
				variablesValues);
		JsonObject json = QuerytoJSON.parseAssayDetails(results);
		System.out.println("AssayTypeDetails " + studyID +" "+ measurement+" "+technology+" in "
				+ (System.currentTimeMillis() - time));
		return json;
	}

	public void deleteStudy(String studyID, boolean investigationDelete) {
		long time = System.currentTimeMillis();
		if (!investigationDelete) {
			String investigationID = getInvestigationIDFromStudyID(studyID);
			if (investigationID != null) {
				JsonObject studies = getInvestigationStudiesBrowseView(investigationID);
				JsonArray arr = studies.get("i_studies").getAsJsonArray();
				if (arr.size() == 1) {
					deleteInvestigation(investigationID);
					return;
				}
			}
		}

		String uri = getStudyID_Uri(studyID);
		if (uri == null)
			return;
		// SailGraph sailgraph=GraphDatabase.lockSailGraph();
		GraphDatabase.removeVertex(uri);
		// GraphDatabase.unlockSailGraph();
		System.out.println("Delete Study " + studyID + " in "
				+ (System.currentTimeMillis() - time));
	}

	private String getInvestigationIDFromStudyID(String studyID) {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		QueryResultsIterator results = runQuery(PREFIX,
				INVESTIGATION_ID_FROM_STUDY_ID, variablesValues);
		try {
			if (!results.hasNext())
				return null;
			else {
				QueryResult res = results.next();
				return res.getBinding("i_id").getValue();
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return null;

	}

	public String getInvestigationID_Uri(String investigationID) {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", investigationID);
		QueryResultsIterator results = runQuery(PREFIX, INVESTIGATION_ID_URI,
				variablesValues);
		try {
			if (results.hasNext()) {
				QueryResult res = results.next();
				String name = res.getBindingNames().iterator().next();
				return res.getBinding(name).getValue();
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return null;
	}

	private String getStudyID_Uri(String studyID) {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", studyID);
		QueryResultsIterator results = runQuery(PREFIX, STUDY_ID_URI,
				variablesValues);
		try {
			if (results.hasNext()) {
				QueryResult res = results.next();
				String name = res.getBindingNames().iterator().next();
				return res.getBinding(name).getValue();
			}
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void updateInvestigation(String i_id, String field, String value) {
		String queryString;
		
		value=value.replace("\n","\\n");
		value=value.replace("\t","\\t");
		value=value.replace("\r","\\r");
		value=value.replace("\b","\\b");
		value=value.replace("\f","\\f");
		value=value.replace("\'","\\'");
		value=value.replace("\"","\\\"");
		
		if (field.equals("i_id")) {
			queryString = parseFilesToString(PREFIX,
					UPDATE_INVESTIGATION_ID_QUERY);
			queryString = queryString.replace("$i_id", i_id);
			queryString = queryString.replace("$value", value);
			runUpdateQuery(queryString);
		}

		queryString = parseFilesToString(PREFIX, UPDATE_INVESTIGATION_QUERY);

		queryString = queryString.replace("$i_id", i_id);
		queryString = queryString.replace("$value", value);
		queryString = queryString.replace("$field_type", IRI_MAPPING.get(field)
				.getAsJsonObject().get("type").getAsString());
		queryString = queryString.replace("$label", IRI_MAPPING.get(field)
				.getAsJsonObject().get("label").getAsString());
		runUpdateQuery(queryString);
	}

	public void updateStudy(String s_id, String field, String value) {
		
		String queryString;
		value=value.replaceAll("\n","\\n");
		value=value.replaceAll("\t","\\t");
		value=value.replaceAll("\r","\\r");
		value=value.replaceAll("\b","\\b");
		value=value.replaceAll("\f","\\f");
		value=value.replaceAll("\'","\\'");
		value=value.replaceAll("\"","\\\"");
		
		if (field.equals("s_id")) {
			queryString = parseFilesToString(PREFIX, UPDATE_STUDY_ID_QUERY);
			queryString = queryString.replace("$s_id", s_id);
			queryString = queryString.replace("$value", value);
			runUpdateQuery(queryString);
		}

		queryString = parseFilesToString(PREFIX, UPDATE_STUDY_QUERY);

		queryString = queryString.replace("$s_id", s_id);
		queryString = queryString.replace("$value", value);
		queryString = queryString.replace("$field_type", IRI_MAPPING.get(field)
				.getAsJsonObject().get("type").getAsString());
		queryString = queryString.replace("$label", IRI_MAPPING.get(field)
				.getAsJsonObject().get("label").getAsString());
		runUpdateQuery(queryString);
	}

	public String updatePublication(String id, String field, String value) {
		return "Not Available Yet";
	}

	public String updatePerson(String id, String field, String value) {
		return "Not Available Yet";
	}

	public int existsID(String id) throws QueryEvaluationException {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", id);
		QueryResultsIterator results = runQuery(PREFIX, INVESTIGATION_ID,
				variablesValues);
		if (results.hasNext())
			return 1;

		variablesValues.put("$s_id", id);
		results = runQuery(PREFIX, STUDY_ID, variablesValues);
		if (results.hasNext())
			return 2;

		return -1;
	}

	public String parseFileToString(String filepath) {
		String str = "";
		BufferedReader reader = null;
		try {
			InputStreamReader is = new InputStreamReader(
					Query.class.getResourceAsStream(filepath));
			reader = new BufferedReader(is);
			String line = "";
			while ((line = reader.readLine()) != null) {
				str += line + "\n";
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

    /**
     *
     *
     * @param prefixpath
     * @param filepath
     * @return
     */
	public String parseFilesToString(String prefixpath, String filepath) {
		String str = "";
		BufferedReader reader = null;
		try {

			InputStreamReader is = new InputStreamReader(
					Query.class.getResourceAsStream(prefixpath));
			reader = new BufferedReader(is);
			String line = "";
			while ((line = reader.readLine()) != null) {
				str += line + "\n";
			}

			is = new InputStreamReader(
					Query.class.getResourceAsStream(filepath));
			reader = new BufferedReader(is);
			line = "";
			while ((line = reader.readLine()) != null) {
				str += line + "\n";
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return str;
	}

	public void printQueryResults(QueryResultsIterator sparqlResults) {
		System.out.println("Query results...");
		int count=0;
		try {
			while (sparqlResults.hasNext()) {
				count++;
				QueryResult res = sparqlResults.next();

				Set<String> names = res.getBindingNames();

				for (String name : names) {
					QueryBinding x = res.getBinding(name);
					String value = x.getValue();
					System.out.println("name, value = " + name + "," + value);
				}// for
				System.out.println();

			}// while
		} catch (QueryEvaluationException e) {
			e.printStackTrace();
		}

		System.out.println("... end of query results. total="+count);
	}
}