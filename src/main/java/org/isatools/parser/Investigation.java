package org.isatools.parser;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class Investigation {

	JsonObject investigation;
	JsonArray i_studies;

	public Investigation() {
		investigation = new JsonObject();
		i_studies = new JsonArray();
		investigation.add("i_studies", i_studies);
	}

	public JsonObject getStudy(String sid) {
		for (JsonElement study : i_studies) {
			JsonObject sobj = study.getAsJsonObject();
			if (sobj.get("s_id").getAsString().equals(sid)) {
				return sobj;
			}
		}
		return null;
	}

	public void addProperty(String name, String value) {
		investigation.addProperty(name, value);
	}

	public void add(String name, JsonElement element) {
		investigation.add(name, element);
	}

	public void addStudy(Study study) {
		i_studies.add(study.getStudy());
	}

	public boolean has(String name) {
		return investigation.has(name);
	}

}