package org.isatools.bii.benchmarking;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.apache.commons.io.FileDeleteStrategy;
import org.isatools.database.GremlinQuery;
import org.isatools.database.SparqlQuery;
import org.isatools.database.structures.QueryResultsIterator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;
import com.tinkerpop.gremlin.groovy.GremlinGroovyPipeline;

/**
 * Created by the ISATeam. User: agbeltran Date: 27/06/2013 Time: 16:48
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
@RunWith(value = Parameterized.class)
public class QueryBenchmarkNoIteration {

	private static final int QUERY_EVAL_RUN_TIMES = 10;
	private boolean PRINT_RESULTS = false;
	private static SailConnection neo4jSailConnection;
	private static SailConnection tinkerSailConnection;
	private static SailConnection orientSailConnection;
	private static String baseURI;
	private static ScriptEngine neo4jEngine;
	private static ScriptEngine tinkerEngine;
	private static ScriptEngine orientEngine;
	private static String[] sparqlQuery;
	private static String[] gremlinScript;
	private static Map<String, String> variablesValues = new HashMap<String, String>();
	private static SparqlQuery sparqlQueryObj;
	private static GremlinQuery gremlinQueryObj;

	@BeforeClass
	public static void testLoading() throws SailException, IOException {
		File benchmark = new File(QueryBenchmarkNoIteration.class.getClass()
				.getResource("/benchmark").getFile());
		for (File f : benchmark.listFiles()) {
			if (!f.getName().equals("create.txt"))
				FileDeleteStrategy.FORCE.delete(f);
		}

		Neo4jGraph neo4jGraph = new Neo4jGraph(benchmark.getPath() + "/neo4j");
		OrientGraph orientGraph = new OrientGraph("local:"
				+ benchmark.getPath() + "/orientGraph");
		TinkerGraph tinkerGraph = new TinkerGraph(benchmark.getPath()
				+ "/tinkergraph");

		GraphSail<Neo4jGraph> neo4jSail = new GraphSail<Neo4jGraph>(neo4jGraph);
		GraphSail<TinkerGraph> tinkerSail = new GraphSail<TinkerGraph>(
				tinkerGraph);
		GraphSail<OrientGraph> orientSail = new GraphSail<OrientGraph>(
				orientGraph);

		Sail neo4jReasoner = new ForwardChainingRDFSInferencer(neo4jSail);
		Sail tinkerReasoner = new ForwardChainingRDFSInferencer(tinkerSail);
		Sail orientReasoner = new ForwardChainingRDFSInferencer(orientSail);

		SailGraph neo4jSailGraph = new SailGraph(neo4jSail);
		SailGraph tinkerSailGraph = new SailGraph(tinkerSail);
		SailGraph orientSailGraph = new SailGraph(orientSail);

		neo4jSailConnection = neo4jReasoner.getConnection();
		tinkerSailConnection = tinkerReasoner.getConnection();
		orientSailConnection = orientReasoner.getConnection();

		baseURI = "http://isa-tools.org/isa/";
		neo4jSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/BII-I-1.owl"), baseURI, "rdf-xml",
				null);
		neo4jSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		tinkerSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		orientSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/E-GEOD-25835-MPBRCA1.owl"),
				baseURI, "rdf-xml", null);
		neo4jSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		tinkerSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);
		orientSailGraph.loadRDF(QueryBenchmarkNoIteration.class
				.getResourceAsStream("/data/MTBLS2.owl"), baseURI, "rdf-xml",
				null);

		ScriptEngineManager manager = new ScriptEngineManager();
		neo4jEngine = manager.getEngineByName("gremlin-groovy");
		neo4jEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				neo4jSailGraph);

		tinkerEngine = manager.getEngineByName("gremlin-groovy");
		tinkerEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				tinkerSailGraph);

		orientEngine = manager.getEngineByName("gremlin-groovy");
		orientEngine.getBindings(ScriptContext.ENGINE_SCOPE).put("g",
				orientSailGraph);

		neo4jGraph.getVertices();
		neo4jGraph.getEdges();
		orientGraph.getVertices();
		orientGraph.getEdges();
		tinkerGraph.getVertices();
		tinkerGraph.getEdges();
		variablesValues.put("$i_id", "BII-I-1");
		variablesValues.put("$s_id", "BII-S-1");
	}

	public QueryBenchmarkNoIteration(String[] sparqlQuery,
			String[] gremlinScript) {
		this.sparqlQuery = sparqlQuery;
		this.gremlinScript = gremlinScript;
	}

	@Parameters
	public static Collection<Object[]> data() {
		sparqlQueryObj = new SparqlQuery();
		gremlinQueryObj = new GremlinQuery();
		Object[][] data = new Object[][] {
				{ new String[] { sparqlQueryObj.BROWSE_QUERY },
						new String[] { gremlinQueryObj.BROWSE_QUERY } },
				{ new String[] { sparqlQueryObj.INVESTIGATION_QUERY },
						new String[] { gremlinQueryObj.INVESTIGATION_QUERY } },
				{
						new String[] { sparqlQueryObj.STUDY_QUERY,
								sparqlQueryObj.STUDY_CONTACTS_QUERY,
								sparqlQueryObj.STUDY_GROUPS_COUNT },
						new String[] { gremlinQueryObj.STUDY_QUERY,
								gremlinQueryObj.STUDY_CONTACTS_QUERY,
								gremlinQueryObj.STUDY_GROUPS_COUNT }, },
//				{
//						new String[] { sparqlQueryObj.MATERIAL_DERIVED_FROM_SOURCE, },
//						new String[] { gremlinQueryObj.MATERIAL_DERIVED_FROM_SOURCE, }, }, 
									};
		return Arrays.asList(data);
	}

	@Test
	public void testQuery() throws ScriptException, MalformedQueryException,
			SailException {

		HashMap<String, String> parsedScripts = new HashMap<String, String>();
		HashMap<String, String> parsedQueries = new HashMap<String, String>();

		for (String script : gremlinScript) {
			String parsedScript = gremlinQueryObj.parseFilesToString(
					gremlinQueryObj.PREFIX, script);
			for (String variable : variablesValues.keySet())
				parsedScript = parsedScript.replace(variable,
						variablesValues.get(variable));
			String scriptName = script.substring(script.lastIndexOf("/") + 1);
			parsedScripts.put(scriptName, parsedScript);
		}

		SPARQLParser parser = new SPARQLParser();
		for (String query : sparqlQuery) {
			String stringQuery = sparqlQueryObj.parseFilesToString(
					sparqlQueryObj.PREFIX, query);
			for (String variable : variablesValues.keySet())
				stringQuery = stringQuery.replace(variable,
						variablesValues.get(variable));
			String queryName = query.substring(query.lastIndexOf("/") + 1);
			parsedQueries.put(queryName, stringQuery);
		}

		long startTime = 0, total = 0;
		int sparqlResSize = 0, gremlinResSize = 0;
		String scriptNames = "";
		String queryNames = "";
		ArrayList<QueryResultsIterator> results = new ArrayList<QueryResultsIterator>();
		QueryResultsIterator innerResult = null;

		/**
		 * Neo4J Performance
		 */
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedScripts.entrySet()) {
				innerResult = new QueryResultsIterator(
						(GremlinGroovyPipeline) tinkerEngine.eval(entry
								.getValue()));
				if (i == 0) {
					results.add(innerResult);
					scriptNames += entry.getKey() + " ";
				}

			}
			total += System.nanoTime() - startTime;
		}

		System.out.println("---------------GREMLIN RESULTS---------------");

		System.out.println("Neo4J " + scriptNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		total = 0;
		queryNames = "";
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedQueries.entrySet()) {
				ParsedQuery parsedQuery = parser.parseQuery(entry.getValue(),
						baseURI);
				innerResult = new QueryResultsIterator(
						neo4jSailConnection.evaluate(
								parsedQuery.getTupleExpr(),
								parsedQuery.getDataset(),
								new EmptyBindingSet(), false));
				if (i == 0) {
					results.add(innerResult);
					queryNames += entry.getKey() + " ";
				}
			}
			total += System.nanoTime() - startTime;
		}

		System.out.println("--------------SPARQL RESULTS-----------------");

		System.out.println("Neo4J " + queryNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		assertEquals(sparqlResSize, gremlinResSize);

		/**
		 * Tinker Graph evalution
		 */
		startTime = 0;
		total = 0;
		scriptNames = "";
		sparqlResSize = 0;
		gremlinResSize = 0;

		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedScripts.entrySet()) {
				innerResult = new QueryResultsIterator(
						(GremlinGroovyPipeline) tinkerEngine.eval(entry
								.getValue()));
				if (i == 0) {
					results.add(innerResult);
					scriptNames += entry.getKey() + " ";
				}
			}

			total += System.nanoTime() - startTime;
		}
		System.out.println("---------------GREMLIN RESULTS---------------");

		System.out.println("TinkerGraph " + scriptNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		startTime = 0;
		total = 0;
		queryNames = "";
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedQueries.entrySet()) {
				ParsedQuery parsedQuery = parser.parseQuery(entry.getValue(),
						baseURI);
				innerResult = new QueryResultsIterator(
						tinkerSailConnection.evaluate(
								parsedQuery.getTupleExpr(),
								parsedQuery.getDataset(),
								new EmptyBindingSet(), false));
				if (i == 0) {
					results.add(innerResult);
					queryNames += entry.getKey() + " ";
				}
			}

			total += System.nanoTime() - startTime;
		}
		System.out.println("--------------SPARQL RESULTS-----------------");

		System.out.println("TinkerGraph " + queryNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		assertEquals(sparqlResSize, gremlinResSize);

		/**
		 * Orient Graph evalution
		 */
		startTime = 0;
		total = 0;
		scriptNames = "";
		sparqlResSize = 0;
		gremlinResSize = 0;

		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedScripts.entrySet()) {
				innerResult = new QueryResultsIterator(
						(GremlinGroovyPipeline) orientEngine.eval(entry
								.getValue()));
				if (i == 0) {
					results.add(innerResult);
					scriptNames += entry.getKey() + " ";
				}
			}

			total += System.nanoTime() - startTime;
		}
		System.out.println("---------------GREMLIN RESULTS---------------");

		System.out.println("OrientGraph " + scriptNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		startTime = 0;
		total = 0;
		queryNames = "";
		for (int i = 0; i < QUERY_EVAL_RUN_TIMES; i++) {
			startTime = System.nanoTime();
			for (Entry<String, String> entry : parsedQueries.entrySet()) {
				ParsedQuery parsedQuery = parser.parseQuery(entry.getValue(),
						baseURI);
				innerResult = new QueryResultsIterator(
						orientSailConnection.evaluate(
								parsedQuery.getTupleExpr(),
								parsedQuery.getDataset(),
								new EmptyBindingSet(), false));
				if (i == 0) {
					results.add(innerResult);
					queryNames += entry.getKey() + " ";
				}
			}

			total += System.nanoTime() - startTime;
		}
		System.out.println("--------------SPARQL RESULTS-----------------");
		System.out.println("OrientGraph " + queryNames + " Duration "
				+ (double) ((total / QUERY_EVAL_RUN_TIMES)*0.000001) + "ms");
		System.out.println("---------------------------------------------");

		// assertEquals(sparqlResSize,gremlinResSize);
	}

}