package org.isatools.database.structures;

import info.aduna.iteration.CloseableIteration;

import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryEvaluationException;

import com.tinkerpop.gremlin.groovy.GremlinGroovyPipeline;

public class QueryResultsIterator {

	private CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlIterator;
	private GremlinGroovyPipeline gremlinIterator;

	public QueryResultsIterator(
			CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlIterator) {
		this.sparqlIterator = sparqlIterator;
	}

	public QueryResultsIterator(GremlinGroovyPipeline gremlinIterator) {
		this.gremlinIterator = gremlinIterator;
	}

	public boolean hasNext() throws QueryEvaluationException {
		if (sparqlIterator != null)
			return sparqlIterator.hasNext();
		return gremlinIterator.hasNext();
	}

	public QueryResult next() throws QueryEvaluationException {
		if (sparqlIterator != null)
			return new QueryResult(sparqlIterator.next());
		return new QueryResult(gremlinIterator.next());
	}
	
}
