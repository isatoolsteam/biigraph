**BII Graph Loader and Query Code**

This codebase shows how to load content into Graph databases using Tinkerpop Blueprints. We also show how to load ISA RDF into a graph database and how to query these graphs using SPAQRL.

**Links:**

[Linked Data and SAIL](https://github.com/joshsh/ripple/wiki/LinkedDataSail])

[SAIL Ouplementation](https://github.com/tinkerpop/blueprints/wiki/Sail-Ouplementation)

[SAIL and Gremlin](http://danbri.org/words/2011/05/10/675)