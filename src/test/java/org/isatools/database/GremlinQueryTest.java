package org.isatools.database;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.isatools.database.structures.QueryResultsIterator;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;

import com.google.gson.JsonObject;

/**
 * Created by the ISATeam. User: agbeltran Date: 16/07/2013 Time: 10:55
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
public class GremlinQueryTest {

	private Query query;

	@Before
	public void testLoading() throws SailException, FileNotFoundException,
			MalformedURLException, IOException {
		query = new GremlinQuery();
	}

	// BROWSE
	@Test
	public void getBrowseInfoTest() {
		GremlinQuery query = new GremlinQuery();
		JsonObject json = query.getBrowseInfo();
		System.out.println(json);
	}

	@Test
	public void getAssayTypesCountPerStudyTest()
			throws QueryEvaluationException {
		QueryResultsIterator it = query.runQuery(query.PREFIX,
				query.STUDY_ASSAY_QUERY);
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	@Test
	public void getBrowse2Test() throws QueryEvaluationException {
			query.printQueryResults(query.runQuery(query.PREFIX,
					query.BROWSE_QUERY));
	}

	// INVESTIGATION
	@Test
	public void getInvestigationTest() throws QueryEvaluationException {
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.INVESTIGATION_QUERY));
	}

	// @Test
	public void getInvestigationTest2() {
		// Map<String, String> variablesValues = new HashMap<String, String>();
		// variablesValues.put("$i_id", "BII-I-1");
		// query.printQueryResults(query.runQuery(query.PREFIX,
		// query.INVESTIGATION_QUERY, variablesValues));
	}

	@Test
	public void getContactInvestigationsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$i_id", "BII-I-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.INVESTIGATION_CONTACTS_QUERY, variablesValues));
	}

	// STUDY
	@Test
	public void getStudyTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_QUERY, variablesValues));
	}

	@Test
	public void getStudyInfo() {
		query.getStudyInfo("BII-S-1");
	}

	@Test
	public void getStudyGroupsCountTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_GROUPS_COUNT, variablesValues));
	}

	@Test
	public void getStudyGroupsMembersTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$study_group", "Factor Value[limiting nutrient] FYPO:glucose Factor Value[rate] 0.07 Unit l/hour");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_GROUP_MEMBERS, variablesValues));
	}

	@Test
	public void getStudyContactsTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.STUDY_CONTACTS_QUERY, variablesValues));
	}

	@Test
	public void getDerivedMaterialTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.MATERIAL_DERIVED_FROM_SOURCE, variablesValues));

	}
	
	@Test
	public void getAssayTest() {
		Map<String, String> variablesValues = new HashMap<String, String>();
		variablesValues.put("$s_id", "BII-S-1");
		variablesValues.put("$measurement", "OBI:metabolite profiling");
		variablesValues.put("$technology", "OBI:mass spectrometry");
		query.printQueryResults(query.runQuery(query.PREFIX,
				query.ASSAY_QUERY, variablesValues));

	}

}
