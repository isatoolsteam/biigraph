package org.isatools.database;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;
import info.aduna.iteration.CloseableIteration;
import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.IOUtils;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.TupleExpr;
import org.openrdf.query.algebra.UpdateExpr;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GraphDatabase {

	private static KeyIndexableGraph graph;
	private static GraphSail<KeyIndexableGraph> sail;
	private static ForwardChainingRDFSInferencer reasoner;
	private static SailGraph sailGraph;
	public static JsonObject config;
	private static final Lock lockSailGraph = new ReentrantLock(true);

	static {
		loadConfig();
		String dbpath = System.getProperty("java.io.tmpdir");

		String name = config.get("name").getAsString();
		File file = new File(dbpath + name);

		if (file.exists()) {
			try {
				FileDeleteStrategy.FORCE.delete(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		file.mkdirs();
		file.setExecutable(true, false);
		file.setReadable(true, false);
		file.setWritable(true, false);

		graph = GraphFactory.createGraph(config.get("type").getAsString(),
				name, dbpath);
		sail = new GraphSail<KeyIndexableGraph>(graph);
		sailGraph = new SailGraph(sail);
		reasoner = new ForwardChainingRDFSInferencer(
				new GraphSail<KeyIndexableGraph>(graph));

		try {
			reasoner.initialize();
		} catch (SailException e) {
			e.printStackTrace();
		}
		if (config.get("add_samples").getAsBoolean()) {
			addSampleStudies();
		}

	}

    /**
     * Loads the configuration file (found under /src/main/resources/dbconfig.xml)
     *
     * The configuration file indicates:
     *    - the database type (type)
     *    - the filename to use for the database (name)
     *    - if some initial ISA-TAB files will be loaded (add_samples: boolean)
     *    - the number of ISA-TAB files to be loaded (number_of_samples)
     *    - what ISA-TAB files to load (samples)
     */
	private static void loadConfig() {
		config = new JsonObject();
		InputStream is = GraphDatabase.class
				.getResourceAsStream("/dbconfig.xml");
		String xml;
		try {
			xml = IOUtils.toString(is);
			XMLSerializer xmlSerializer = new XMLSerializer();
			JSON json = xmlSerializer.read(xml);
			JsonObject jsonObj = new JsonParser().parse(json.toString())
					.getAsJsonObject();

			config.addProperty("add_samples", jsonObj.get("add_samples")
					.getAsBoolean());
			config.addProperty("number_of_samples",
					jsonObj.get("number_of_samples").getAsInt());
			config.addProperty("name", jsonObj.get("name").getAsString());
			config.addProperty("type", jsonObj.get("type").getAsString());
			config.add("samples", jsonObj.get("samples").getAsJsonArray());

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

    /**
     * Adds ISA-TAB files to the database,
     * as indicated by the configuration file parameters
     */
	public static void addSampleStudies() {

		int numOfSamples = config.get("number_of_samples").getAsInt();
		int cnt = 0;
		for (JsonElement fileElem : config.get("samples").getAsJsonArray()) {
			if (cnt >= numOfSamples) {
				break;
			}
			String filename = fileElem.getAsString();
			InputStream is = GraphDatabase.class
					.getResourceAsStream("/database/init/" + filename);
			System.out.println("Loading file " + filename);
			loadRDF(is, "http://isa-tools.org/isa/", "rdf-xml", null);
			System.out.println(filename + " loaded!");
			cnt++;
		}
	}

    /**
     *
     * Inner class responsible for creating the Graph Database
     *
     */
	private static class GraphFactory {

		public static KeyIndexableGraph createGraph(String type, String name,
				String directory) {
			System.out.println("createGraph " + type + " " + name + " "
					+ directory);
			if (type.equals("tinkergraph")) {
				return new TinkerGraph(directory + name+"/tinkergraph.dat");
			}
			if (type.equals("orientdb")) {
				return new OrientGraph("local:" + directory + name);
			}
            if (type.equals("neo4j")){
				return new Neo4jGraph(directory + name);
			}
            System.err.println("No database type provided");
            return null;
		}
	}

    /**
     * Rollback method
     */
	public static void rollback() {
		try {
			lockSailGraph.lock();
			System.out.println("rollback has the lock");
			sail.getConnection().rollback();
		} catch (SailException e) {
			e.printStackTrace();
		} finally {
			lockSailGraph.unlock();
			System.out.println("rollback released the lock");
		}
	}

    /**
     * Loads RDF data.
     *
     * @param is InputStream with the RDF data
     * @param baseURI the base IRI of the RDF graph
     * @param format indicates the format (e.g. rdf-xml)
     * @param baseGraph a string indicating the graph to insert the new data into
     */
	public static void loadRDF(InputStream is, String baseURI, String format,
			String baseGraph) {
		try {
			lockSailGraph.lock();
			System.out.println("loadRDF has the lock");
			sailGraph.loadRDF(is, baseURI, format, baseGraph);
		} finally {
			lockSailGraph.unlock();
			System.out.println("loadRDF released the lock");
		}
	}

    /**
     *
     * Executes an SPARQL UPDATE
     *
     * @param updateExpr the SPARQL UPDATE expression
     * @param dataset
     * @param bindings
     * @param includeInferred
     */
	public static void executeSPARQLUpdate(UpdateExpr updateExpr,
			Dataset dataset, EmptyBindingSet bindings, boolean includeInferred) {
		try {
			lockSailGraph.lock();
			sail.getConnection().executeUpdate(updateExpr, dataset, bindings, includeInferred);
		} catch (SailException e) {
			e.printStackTrace();
		} finally {
			lockSailGraph.unlock();
		}

	}

    /**
     * Executes an SPARQL query
     *
     * @param tupleExpr
     * @param dataset
     * @param bindings
     * @param includeInferred
     * @return
     */
	public static CloseableIteration<? extends BindingSet, QueryEvaluationException> evaluateSPARQL(
			TupleExpr tupleExpr, Dataset dataset, EmptyBindingSet bindings,
			boolean includeInferred) {

		CloseableIteration<? extends BindingSet, QueryEvaluationException> results = null;
		try {
//			lockSailGraph.lock();
//			System.out.println("evaluateSPARQL has the lock");
			results = sail.getConnection().evaluate(tupleExpr, dataset,
					bindings, includeInferred);
		} catch (SailException e) {
			e.printStackTrace();
		} finally {
//			lockSailGraph.unlock();
//			System.out.println("evaluateSPARQL released the lock");
		}
		return results;

	}

    /**
     *
     * Evaluates a Gremlin query.
     *
     * @param queryString
     * @return
     */
	public static Object evalGremlin(String queryString) {
		Object results = null;
		ScriptEngine engine = null;
		try {
//			lockSailGraph.lock();
//			System.out.println("evalGremlin has the lock");
			ScriptEngineManager manager = new ScriptEngineManager();
			engine = manager.getEngineByName("gremlin-groovy");
			engine.getBindings(ScriptContext.ENGINE_SCOPE).put("g", sailGraph);
			results = engine.eval(queryString);
		} catch (ScriptException e) {
			e.printStackTrace();
		} finally {
//			lockSailGraph.unlock();
//			System.out.println("evalGremlin released the lock");
		}
		return results;
	}

    /**
     * Removes a specific vertex in the graph
     *
     * @param uri URI identifying the vertex to remove
     */
	public static void removeVertex(String uri) {
		try {
			 lockSailGraph.lock();
			System.out.println("removeVertex has the lock");
			Vertex vertex = sailGraph.getVertex(uri);
			sailGraph.removeVertex(vertex);
			System.out.println("Removing verted " + vertex);
		} finally {
			 lockSailGraph.unlock();
			System.out.println("removeVertex released the lock");
		}
	}

}
