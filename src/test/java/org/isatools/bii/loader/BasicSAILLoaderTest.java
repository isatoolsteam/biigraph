package org.isatools.bii.loader;

import info.aduna.iteration.CloseableIteration;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Graph;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

/**
 * Initial Simple Test for RDF import and query.
 * User: eamonnmaguire
 * Date: 29/05/2013
 * Time: 14:06
 */
public class BasicSAILLoaderTest {

    Resource strabane = new URIImpl("http://isa-tools.org/things/Strabane");
    Resource town = new URIImpl("http://isa-tools.org/terms/town");
    Resource place = new URIImpl("http://isa-tools.org/terms/place");

    private GraphSail<TinkerGraph> sail;
    private TinkerGraph graph;
    private SailConnection sailConnection;

    @Before
    public void testLoading() throws SailException {
        graph = new TinkerGraph();
        sail = new GraphSail<TinkerGraph>(graph);
        sail.initialize();
        sailConnection = sail.getConnection();

        sailConnection.addStatement(town, RDFS.SUBCLASSOF, place);
        sailConnection.addStatement(strabane, RDF.TYPE, town);
        sailConnection.commit();
    }

    @Test
    public void testGetStatements() throws SailException {
        System.out.println("____ START testGetStatements() _____");
        System.out.println("get statements: ?s ?p ?o ?g");
        CloseableIteration<? extends Statement, SailException> results = sailConnection.getStatements(null, null, null, false);
        while (results.hasNext()) {
            System.out.println(results.next());
        }

        results = sailConnection.getStatements(RDF.TYPE, null, null, false);
        while (results.hasNext()) {
            System.out.println(results.next());
        }
        System.out.println("____ END testGetStatements() _____");
    }

    @Test
    public void testSPARQLQuery() throws SailException, MalformedQueryException, QueryEvaluationException {
        System.out.println("____ START testSPARQLQuery() _____");
        SPARQLParser parser = new SPARQLParser();
        CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults;
        String queryString = "SELECT ?x ?y WHERE { ?x <" + RDF.TYPE + "> ?y }";
        ParsedQuery query = parser.parseQuery(queryString, "http://tinkerpop.com");

        System.out.println("\nSPARQL: " + queryString);
        sparqlResults = sailConnection.evaluate(query.getTupleExpr(), query.getDataset(), new EmptyBindingSet(), false);
        while (sparqlResults.hasNext()) {
            System.out.println(sparqlResults.next());
        }

        System.out.println("____ END testSPARQLQuery() _____");
    }

    @Test
    public void testGetStatementsAsGraph() {
        System.out.println("____ START testGetStatementsAsGraph() _____");
        Graph graph = ((GraphSail) sail).getBaseGraph();
        System.out.println();
        for (Vertex v : graph.getVertices()) {
            System.out.println("------");
            System.out.println(v);
            for (String key : v.getPropertyKeys()) {
                System.out.println(key + "=" + v.getProperty(key));
            }
        }
        for (Edge e : graph.getEdges()) {
            System.out.println("------");
            System.out.println(e);
            for (String key : e.getPropertyKeys()) {
                System.out.println(key + "=" + e.getProperty(key));
            }
        }
        System.out.println("____ END testGetStatementsAsGraph() _____");
    }

    @Test
    public void testInferenceSupport() throws SailException {
        System.out.println("____ START testInferenceSupport() _____");

        Sail reasoner = new ForwardChainingRDFSInferencer(new GraphSail<TinkerGraph>(graph));
        reasoner.initialize();

        CloseableIteration<? extends Statement, SailException> i
                = sailConnection.getStatements(strabane, null, null, true);
        try {
            while (i.hasNext()) {
                System.out.println("statement " + i.next());
            }
        } finally {
            i.close();
        }
        System.out.println("____ END testInferenceSupport() _____");

    }

    @After
    public void cleanupAndShutDown() {
        try {
            sailConnection.close();
            graph.shutdown();
            sail.shutDown();
        } catch (SailException e) {
            e.printStackTrace();
        }

    }

}
