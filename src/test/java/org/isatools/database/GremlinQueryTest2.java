package org.isatools.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import org.isatools.bii.benchmarking.QueryBenchmark;
import org.isatools.database.structures.QueryResultsIterator;
import org.junit.Before;
import org.junit.Test;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import com.google.gson.JsonObject;
import com.tinkerpop.blueprints.KeyIndexableGraph;
import com.tinkerpop.blueprints.impls.sail.SailGraph;
import com.tinkerpop.blueprints.impls.sail.impls.LinkedDataSailGraph;
import com.tinkerpop.blueprints.impls.tg.TinkerGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;
import com.tinkerpop.gremlin.groovy.GremlinGroovyPipeline;

/**
 * Created by the ISATeam. User: agbeltran Date: 16/07/2013 Time: 10:55
 * 
 * @author <a href="mailto:alejandra.gonzalez.beltran@gmail.com">Alejandra
 *         Gonzalez-Beltran</a>
 */
public class GremlinQueryTest2 {

	private GraphSail<TinkerGraph> sail;
	private TinkerGraph graph;
	private SailConnection sailConnection;
	private SailGraph sailGraph;
	private String baseURI;
	private LinkedDataSailGraph gremlin;
	private ScriptEngine engine;
	private Query query;

	@Before
	public void testLoading() throws SailException, FileNotFoundException,
			MalformedURLException, IOException {
		new File("/tmp/graph/").delete();
		new File("/tmp/graph/").mkdirs();
		graph = new TinkerGraph();
		sail = new GraphSail<TinkerGraph>(graph);

		sailGraph = new SailGraph(sail);

		Sail reasoner = new ForwardChainingRDFSInferencer(
				new GraphSail<KeyIndexableGraph>(graph));

		try {
			reasoner.initialize();
		} catch (SailException e) {
			e.printStackTrace();
		}

		sailConnection = sail.getConnection();
		baseURI = "http://isa-tools.org/isa/";

		sailGraph.loadRDF(QueryBenchmark.class
				.getResourceAsStream("/data/merged-obi-comments_v2012-07-01.owl"),
				baseURI, "rdf-xml", null);
		
		sailGraph.loadRDF(
				QueryBenchmark.class.getResourceAsStream("/data/BII-I-1.owl"),
				baseURI, "rdf-xml", null);
		sailGraph.loadRDF(
		QueryBenchmark.class.getResourceAsStream("/data/MTBLS2.owl"),
		baseURI, "rdf-xml", null);
		System.out.println("loading obi");
		// sailGraph.loadRDF(
		// queryBenchmark.class.getResourceAsStream("/data/faahko.owl"),
		// baseURI, "rdf-xml", null);
		ScriptEngineManager manager = new ScriptEngineManager();
		engine = manager.getEngineByName("gremlin-groovy");
		engine.getBindings(ScriptContext.ENGINE_SCOPE).put("g", sailGraph);
		query = new GremlinQuery();
	}

	// BROWSE
	@Test
	public void getBrowseInfoTest() {
		GremlinQuery query = new GremlinQuery();
		JsonObject json = query.getBrowseInfo();
		System.out.println(json);
	}

	@Test
	public void getAssayTypesCountPerStudyTest()
			throws QueryEvaluationException {
		QueryResultsIterator it = query.runQuery(query.PREFIX,
				query.STUDY_ASSAY_QUERY);
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	@Test
	public void getBrowse2Test() throws QueryEvaluationException {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.BROWSE_QUERY);
			QueryResultsIterator ss = new QueryResultsIterator(
					(GremlinGroovyPipeline) engine.eval(script));
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	// INVESTIGATION
	@Test
	public void getInvestigationTest() throws QueryEvaluationException {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.INVESTIGATION_QUERY);
			script = script.replace("$i_id", "BII-I-1");
			QueryResultsIterator ss = new QueryResultsIterator(
					(GremlinGroovyPipeline) engine.eval(script));
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void getInvestigationTest2() {
		// Map<String, String> variablesValues = new HashMap<String, String>();
		// variablesValues.put("$i_id", "BII-I-1");
		// query.printQueryResults(query.runQuery(query.PREFIX,
		// query.INVESTIGATION_QUERY, variablesValues));
	}

	 @Test
	public void getContactInvestigationsTest() {
		 Map<String, String> variablesValues = new HashMap<String, String>();
		 variablesValues.put("$i_id", "BII-I-1");
		 query.printQueryResults(query.runQuery(query.PREFIX,
		 query.INVESTIGATION_CONTACTS_QUERY, variablesValues));
	}

	// STUDY
	 @Test
	public void getStudyTest() {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.STUDY_QUERY);
			script = script.replace("$s_id", "BII-S-1");
			GremlinGroovyPipeline ss = (GremlinGroovyPipeline) engine
					.eval(script);
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getStudyInfo() {
		query.getStudyInfo("BII-S-1");
	}

	@Test
	public void getStudyGroupsCountTest() {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.STUDY_GROUPS_COUNT);
			script = script.replace("$s_id", "BII-S-1");
			GremlinGroovyPipeline ss = (GremlinGroovyPipeline) engine
					.eval(script);
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void getStudyGroupsMembersTest() {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.STUDY_GROUP_MEMBERS);
			script = script
					.replace("$study_group",
							"Factor Value[limiting nutrient] RID:sulfur Factor Value[rate] 0.1 Unit l/hour");
			GremlinGroovyPipeline ss = (GremlinGroovyPipeline) engine
					.eval(script);
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getStudyContactsTest() {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.STUDY_CONTACTS_QUERY);
			script = script
					.replace("$s_id","BII-S-1");
			GremlinGroovyPipeline ss = (GremlinGroovyPipeline) engine
					.eval(script);
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void getDerivedMaterialTest() {
		try {
			String script = query.parseFilesToString(query.PREFIX,
					query.MATERIAL_DERIVED_FROM_SOURCE);
			script = script
					.replace("$s_id","BII-S-1");
			GremlinGroovyPipeline ss = (GremlinGroovyPipeline) engine
					.eval(script);
			while (ss.hasNext())
				System.out.println(ss.next());
		} catch (ScriptException e) {
			e.printStackTrace();
		}
	}

}
