package org.isatools.database;

import info.aduna.iteration.CloseableIteration;

import org.isatools.database.structures.QueryResultsIterator;
import org.openrdf.query.BindingSet;
import org.openrdf.query.Dataset;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.algebra.UpdateExpr;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.ParsedUpdate;
import org.openrdf.query.parser.sparql.SPARQLParser;

public class SparqlQuery extends Query {
	
	public final String QUERY_PATH;

	public SparqlQuery() {
		super("/database/queries/sparql_queries/", ".sparql");
		this.QUERY_PATH="/database/queries/sparql_queries/";
	}

	@Override
	public QueryResultsIterator runQuery(
			String queryString) {
		SPARQLParser parser = new SPARQLParser();
		CloseableIteration<? extends BindingSet, QueryEvaluationException> sparqlResults = null;
		ParsedQuery query;
		try {
			query = parser.parseQuery(queryString, baseURI);
			sparqlResults = GraphDatabase.evaluateSPARQL(
					query.getTupleExpr(), query.getDataset(),
					new EmptyBindingSet(), true);
		} catch (MalformedQueryException e) {
			e.printStackTrace();
		}

		return new QueryResultsIterator(sparqlResults);
	}
	
	
	@Override
	public void runUpdateQuery(String queryString) {
		SPARQLParser parser = new SPARQLParser();
		ParsedUpdate query = null;
		try {
			query = parser.parseUpdate(queryString, baseURI);
			UpdateExpr expr = query.getUpdateExprs().get(0);
			Dataset dataset = query.getDatasetMapping().get(expr);
			GraphDatabase.executeSPARQLUpdate(expr, dataset,
					new EmptyBindingSet(), false);
		} catch (MalformedQueryException e) {
			GraphDatabase.rollback();
			e.printStackTrace();
		}

	}


}